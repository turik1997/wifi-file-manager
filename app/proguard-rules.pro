# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in <home_dir>/<user>/Android/Sdk/tools/proguard/proguard-android.txt
# You can edit the include fullName and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:
#   Gson-specific: https://stackoverflow.com/questions/57790047/getting-java-lang-assertionerror-java-lang-nosuchfieldexception-http-1-0

# Prevent R8 from removing the InitializationProvider.
# Also, ensure the InitializationProvider is included in the primary dex file
-keepnames class * extends androidx.startup.Initializer
# These Proguard rules ensures that ComponentInitializers are are neither shrunk nor obfuscated,
# and are a part of the primary dex file. This is because they are discovered and instantiated
# during application startup.
-keep class * extends androidx.startup.Initializer {
    # Keep the public no-argument constructor while allowing other methods to be optimized.
    <init>();
}

-keepclassmembers enum * { *; }
-keep class com.google.code.gson.* { *; }
-keepattributes *Annotation*, Signature, Exception
-keepclassmembers,allowobfuscation class * {
  @com.google.gson.annotations.SerializedName <fields>;
}

# Project-specific
-keep class com.chiefcomma.wfm.request.** { *; }
-keep class com.chiefcomma.wfm.response.** { *; }
-keep class com.chiefcomma.wfm.webservice.api.response.** { *; }
-keep class com.chiefcomma.wfm.webservice.NodeData { *; }
-keep class com.chiefcomma.wfm.tasks.TaskProgress { private *; }
-keep class com.chiefcomma.wfm.tasks.Description { private *; }
-keep class com.chiefcomma.wfm.tasks.DescriptionLine { private *; }

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
-keepattributes LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
-renamesourcefileattribute SourceFile
-optimizationpasses 10
#-dontobfuscate
#-verbose
#-printusage
-keep class io.reactivex.** { *; }