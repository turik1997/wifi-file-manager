package com.chiefcomma.app.storage;

import java.io.File;

import android.content.Context;

public final class LegacyStorageClassification {

    private static final String SECONDARY_STORAGES = System.getenv("SECONDARY_STORAGE");

    /** Retrofit of {@link android.os.storage.StorageVolume#getDescription(Context)} to older apis */
    public static StorageType getStorageTypeLegacy(File file) {
        String path = file.getPath();
        if (!path.endsWith("/")) {
            path += "/";
        }
        if (path.equals("/storage/emulated/legacy/")
                || path.equals("/storage/emulated/0/")
                || path.equals("/mnt/sdcard/")
        ) {
            return StorageType.INTERNAL;
        }

        if (path.startsWith("/storage/sdcard")) {
            return StorageType.SD_CARD;
        }

        if (SECONDARY_STORAGES != null) {
            for (String ss : SECONDARY_STORAGES.split(File.pathSeparator)) {
                if (!ss.endsWith("/")) {
                    ss = ss + "/";
                }
                if (path.equals(ss)) {
                    if (path.contains("usb")) {
                        return StorageType.USB;
                    }
                    return StorageType.SD_CARD;
                }
            }
        }
        return StorageType.UNKNOWN;
    }
}
