package com.chiefcomma.app.storage.repository;

import android.os.storage.StorageVolume;

import com.chiefcomma.app.storage.StorageType;

public class WfmStorageVolume {

    private StorageType storageType;
    private Boolean primary;
    private Boolean emulated;
    private Boolean removable;
    private String absolutePath;
    private String description;
    private String userLabel;
    private Long totalSpace;
    private Long occupiedSpace;
    private StorageVolume storageVolume;

    public Boolean getPrimary() {
        return primary;
    }

    public void setPrimary(Boolean primary) {
        this.primary = primary;
    }

    public Boolean getEmulated() {
        return emulated;
    }

    public void setEmulated(Boolean emulated) {
        this.emulated = emulated;
    }

    public String getAbsolutePath() {
        return absolutePath;
    }

    public void setAbsolutePath(String absolutePath) {
        this.absolutePath = absolutePath;
    }

    public Boolean getRemovable() {
        return removable;
    }

    public void setRemovable(Boolean removable) {
        this.removable = removable;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public StorageType getStorageType() {
        return storageType;
    }

    public void setStorageType(StorageType storageType) {
        this.storageType = storageType;
    }

    public Long getTotalSpace() {
        return totalSpace;
    }

    public void setTotalSpace(Long totalSpace) {
        this.totalSpace = totalSpace;
    }

    public Long getOccupiedSpace() {
        return occupiedSpace;
    }

    public void setOccupiedSpace(Long occupiedSpace) {
        this.occupiedSpace = occupiedSpace;
    }

    public String getUserLabel() {
        return userLabel;
    }

    public void setUserLabel(String userLabel) {
        this.userLabel = userLabel;
    }

    public StorageVolume getStorageVolume() {
        return storageVolume;
    }

    public void setStorageVolume(StorageVolume storageVolume) {
        this.storageVolume = storageVolume;
    }
}
