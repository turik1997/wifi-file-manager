package com.chiefcomma.app.storage.repository;

import static com.chiefcomma.app.utils.ReflectionUtil.callMethod;
import static com.chiefcomma.app.utils.ReflectionUtil.callStaticMethod;
import static com.chiefcomma.app.utils.ReflectionUtil.readField;
import static com.chiefcomma.app.utils.ReflectionUtil.withFallbacks;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;

import androidx.annotation.NonNull;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class AndroidStorageRepository {

    @Inject
    public AndroidStorageRepository() {
    }

    public List<StorageVolume> storageVolumeList(Context applicationContext) {
        StorageManager storageManager = getStorageManager(applicationContext);
        try {
            return getStorageVolumes(storageManager);
        } catch (Exception exception) {
            return Collections.emptyList();
        }
    }

    public String getUserLabel(StorageVolume storageVolume) {
        return (String) withFallbacks(
                () -> callMethod(storageVolume, "getUserLabel"),
                () -> readField(storageVolume, "mUserLabel")
        );
    }

    public String getDescription(
            StorageVolume storageVolume,
            Context applicationContext
    ) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return storageVolume.getDescription(applicationContext);
        }
        return (String) withFallbacks(
                () -> callMethod(
                        storageVolume,
                        "getDescription",
                        new Class[] {Context.class},
                        new Object[] {applicationContext}
                ),
                () -> callMethod(storageVolume, "getDescription"),
                () -> readField(storageVolume, "mDescription")
        );
    }

    public Boolean isRemovable(StorageVolume storageVolume) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return storageVolume.isRemovable();
        }
        return (Boolean) withFallbacks(
                () -> callMethod(storageVolume, "isRemovable"),
                () -> readField(storageVolume, "mRemovable")
        );
    }

    @SuppressLint("NewApi")
    public Boolean isPrimary(StorageVolume storageVolume) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return storageVolume.isPrimary();
        }
        Object result = withFallbacks(
                () -> callMethod(storageVolume, "isPrimary"),
                () -> readField(storageVolume, "mPrimary")
        );
        if (result instanceof Boolean) {
            return (Boolean) result;
        }
        result = withFallbacks(() -> callStaticMethod(Environment.class, "getPrimaryVolume"));
        String path = getPath(storageVolume);
        if (path != null) {
            return path.equalsIgnoreCase(getPath((StorageVolume) result));
        }
        return null;
    }

    public Boolean isEmulated(StorageVolume storageVolume) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return storageVolume.isEmulated();
        }
        return (Boolean) withFallbacks(
                () -> callMethod(storageVolume, "isEmulated"),
                () -> readField(storageVolume, "mEmulated")
        );
    }

    public String getPath(StorageVolume storageVolume) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            return storageVolume.getDirectory().getAbsolutePath();
        }
        Object result = withFallbacks(
                () -> callMethod(storageVolume, "getDirectory"),
                () -> callMethod(storageVolume, "getPathFile"),
                () -> callMethod(storageVolume, "getPath"),
                () -> readField(storageVolume, "mPath")
        );
        if (result == null) {
            return null;
        }
        if (result instanceof File) {
            return ((File) result).getAbsolutePath();
        }
        return result.toString();
    }

    private StorageManager getStorageManager(Context applicationContext) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return applicationContext.getSystemService(StorageManager.class);
        }
        return (StorageManager) applicationContext.getSystemService(Context.STORAGE_SERVICE);
    }

    @NonNull
    private List<StorageVolume> getStorageVolumes(StorageManager storageManager) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return storageManager.getStorageVolumes();
        }
        Object result = withFallbacks(
                () -> callMethod(storageManager, "getVolumeList")
        );
        if (result == null) {
            result = new StorageVolume[0];
        }
        return Arrays.asList((StorageVolume[]) result);
    }

}
