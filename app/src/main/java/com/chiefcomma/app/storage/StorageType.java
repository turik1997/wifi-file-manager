package com.chiefcomma.app.storage;

public enum StorageType {
    INTERNAL,
    SD_CARD,
    USB,
    UNKNOWN
}
