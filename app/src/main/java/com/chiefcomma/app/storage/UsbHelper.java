package com.chiefcomma.app.storage;

public class UsbHelper {

    public static boolean containsUsbInPath(String path) {
        // inspired by https://stackoverflow.com/a/49567524/6657837
        return path.contains("usb")
                || path.contains("USB")
                || path.contains("Usb");
    }

    public static String getDescription(String path) {
        int startIndex = path.indexOf("usb");
        if (startIndex == -1) {
            startIndex = path.indexOf("USB");
        }
        if (startIndex == -1) {
            startIndex = path.indexOf("Usb");
        }
        if (startIndex == -1) {
            return null;
        }
        int usbSegmentEndsAt = path.indexOf("/", startIndex);
        return path.substring(startIndex+3, usbSegmentEndsAt);
    }
}
