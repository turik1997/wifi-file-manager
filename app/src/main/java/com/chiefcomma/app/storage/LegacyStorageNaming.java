package com.chiefcomma.app.storage;

import android.content.Context;
import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LegacyStorageNaming {

    private static final Pattern lastIntPattern = Pattern.compile("([0-9]+)$");


    public static final String getLegacyStorageDescription(File file, Context applicationContext) {
        StorageType storageTypeLegacy = LegacyStorageClassification.getStorageTypeLegacy(file);
        switch (storageTypeLegacy) {
            case INTERNAL:
                return "Internal storage";
            case SD_CARD: {
                String path = file.getPath();
                if (path.endsWith("/")) {
                    path = path.substring(0, path.length()-1);
                }
                Matcher matcher = lastIntPattern.matcher(path);
                if (matcher.find()) {
                    String someNumberStr = matcher.group(1);
                    return "SD Card #" + someNumberStr;
                }

                return "SD Card";
            }
            case USB: {
                String description = UsbHelper.getDescription(file.getPath());
                if (description != null) {
                    return description;
                }
                return "USB";
            }
            default:
                return "Unknown Storage";
        }
    }
}
