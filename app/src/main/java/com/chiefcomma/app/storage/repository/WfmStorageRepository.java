package com.chiefcomma.app.storage.repository;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.os.storage.StorageVolume;

import androidx.annotation.Nullable;

import com.chiefcomma.app.storage.LegacyStorageClassification;
import com.chiefcomma.app.storage.LegacyStorageNaming;
import com.chiefcomma.app.storage.StorageType;
import com.chiefcomma.app.storage.UsbHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.hilt.android.qualifiers.ApplicationContext;
import hendrawd.storageutil.library.StorageUtil;

@Singleton
public class WfmStorageRepository {

    private final AndroidStorageRepository androidStorageRepository;
    private final Context applicationContext;
    // Primary emulated SD-CARD
    private static final String EMULATED_STORAGE_SOURCE = System.getenv("EMULATED_STORAGE_SOURCE");
    // All Secondary SD-CARDs (all exclude primary) separated by File.pathSeparator, i.e: ":", ";"
    private static final String SECONDARY_STORAGES = System.getenv("SECONDARY_STORAGE");

    @Inject
    public WfmStorageRepository(
            AndroidStorageRepository androidStorageRepository,
            @ApplicationContext Context applicationContext
    ) {
        this.androidStorageRepository = androidStorageRepository;
        this.applicationContext = applicationContext;
    }

    public List<WfmStorageVolume> storageList() {
        List<WfmStorageVolume> result;
        try {
            List<StorageVolume> storageVolumes = androidStorageRepository.storageVolumeList(applicationContext);
            result = convertStorageVolumesToWfmStorageVolumes(storageVolumes);
        } catch (Exception exception) {
            String[] storageDirectories = StorageUtil.getStorageDirectories(applicationContext);
            result = convertPathsToWfmStorageVolumes(Arrays.asList(storageDirectories));
        }
        List<WfmStorageVolume> filteredResult = new ArrayList<>();
        for (WfmStorageVolume wfmStorageVolume : result) {
            if (wfmStorageVolume.getAbsolutePath() != null) {
                filteredResult.add(wfmStorageVolume);
            }
        }
        return Collections.unmodifiableList(filteredResult);
    }

    private List<WfmStorageVolume> convertStorageVolumesToWfmStorageVolumes(
            List<StorageVolume> storageVolumes
    ) {
        List<WfmStorageVolume> result = new ArrayList<>();
        for (StorageVolume storageVolume : storageVolumes) {
            result.add(convertStorageVolumeToWfmStorageVolume(storageVolume));
        }
        return result;
    }

    private List<WfmStorageVolume> convertPathsToWfmStorageVolumes(List<String> paths) {
        List<WfmStorageVolume> result = new ArrayList<>();
        for (String path : paths) {
            result.add(convertPathToWfmStorageVolume(path));
        }
        return result;
    }

    private WfmStorageVolume convertStorageVolumeToWfmStorageVolume(StorageVolume storageVolume) {
        WfmStorageVolume wfmStorageVolume = new WfmStorageVolume();
        wfmStorageVolume.setStorageVolume(storageVolume);
        wfmStorageVolume.setStorageType(getStorageType(storageVolume));
        wfmStorageVolume.setAbsolutePath(getStorageVolumePath(storageVolume));
        wfmStorageVolume.setEmulated(isEmulated(storageVolume));
        wfmStorageVolume.setPrimary(isPrimary(storageVolume));
        wfmStorageVolume.setRemovable(isRemovable(storageVolume));
        wfmStorageVolume.setDescription(getDescription(storageVolume));
        wfmStorageVolume.setUserLabel(getUserLabel(storageVolume));
        setOccupiedAndTotalSpaces(wfmStorageVolume);
        return wfmStorageVolume;
    }

    private WfmStorageVolume convertPathToWfmStorageVolume(String path) {
        WfmStorageVolume wfmStorageVolume = new WfmStorageVolume();
        wfmStorageVolume.setStorageVolume(null);
        wfmStorageVolume.setAbsolutePath(path);
        File storageVolumeFile = new File(path);
        wfmStorageVolume.setUserLabel(null);
        wfmStorageVolume.setStorageType(LegacyStorageClassification.getStorageTypeLegacy(storageVolumeFile));
        wfmStorageVolume.setEmulated(isEmulatedLegacy(storageVolumeFile));
        wfmStorageVolume.setPrimary(isPrimaryLegacy(storageVolumeFile));
        wfmStorageVolume.setRemovable(isRemovableLegacy(storageVolumeFile));
        wfmStorageVolume.setDescription(getDescriptionLegacy(storageVolumeFile));
        setOccupiedAndTotalSpaces(wfmStorageVolume);
        return wfmStorageVolume;
    }

    private void setOccupiedAndTotalSpaces(WfmStorageVolume wfmStorageVolume) {
        if (wfmStorageVolume.getAbsolutePath() == null) {
            wfmStorageVolume.setOccupiedSpace(null);
            wfmStorageVolume.setTotalSpace(null);
        } else {
            StatFs statFs = new StatFs(wfmStorageVolume.getAbsolutePath());
            wfmStorageVolume.setOccupiedSpace(getOccupiedSpaceLegacy(statFs));
            wfmStorageVolume.setTotalSpace(getTotalSpaceLegacy(statFs));
        }
    }

    private StorageType getStorageType(StorageVolume storageVolume) {
        Boolean primary = androidStorageRepository.isPrimary(storageVolume);
        String path = androidStorageRepository.getPath(storageVolume);
        if (Boolean.TRUE.equals(isRemovable(storageVolume))) {
            if (path != null && UsbHelper.containsUsbInPath(path)) {
                return StorageType.USB;
            }
            return StorageType.SD_CARD;
        }
        if (Boolean.TRUE.equals(primary)) {
            return StorageType.INTERNAL;
        }
        if (path == null) {
            return StorageType.UNKNOWN;
        }
        return LegacyStorageClassification.getStorageTypeLegacy(new File(path));
    }

    @Nullable
    private String getStorageVolumePath(StorageVolume storageVolume) {
        return androidStorageRepository.getPath(storageVolume);
    }

    private String getDescription(StorageVolume storageVolume) {
        String description = androidStorageRepository.getDescription(storageVolume, applicationContext);
        if (description != null) {
            return description;
        }
        String storageVolumePath = getStorageVolumePath(storageVolume);
        return getDescriptionLegacy(new File(storageVolumePath));
    }

    private String getDescriptionLegacy(File file) {
        StorageType storageTypeLegacy = LegacyStorageClassification.getStorageTypeLegacy(file);
        if (storageTypeLegacy != StorageType.UNKNOWN) {
            return LegacyStorageNaming.getLegacyStorageDescription(file, applicationContext);
        }
        if (Environment.getExternalStorageDirectory().getAbsolutePath().startsWith(file.getAbsolutePath())) {
            if (!isRemovableLegacy(file)) {
                return "Internal Storage";
            }
        }
        if (isRemovableLegacy(file)) {
            return "Removable Storage";
        }
        return "Built-in Secondary Storage";
    }

    private Boolean isEmulated(StorageVolume storageVolume) {
        Boolean emulated = androidStorageRepository.isEmulated(storageVolume);
        if (emulated != null) {
            return emulated;
        }
        String storageVolumePath = getStorageVolumePath(storageVolume);
        if (storageVolumePath == null) {
            return false;
        }
        return isEmulatedLegacy(new File(storageVolumePath));
    }

    private boolean isEmulatedLegacy(File storageVolume) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return Environment.isExternalStorageEmulated(storageVolume);
        }
        if (Environment.getExternalStorageDirectory().getAbsolutePath().startsWith(storageVolume.getAbsolutePath())) {
            return Environment.isExternalStorageEmulated();
        }
        if (!isStringEmpty(EMULATED_STORAGE_SOURCE)) {
            for (String emulatedStorage : EMULATED_STORAGE_SOURCE.split(File.pathSeparator)) {
                if (!isStringEmpty(emulatedStorage) && emulatedStorage.startsWith(storageVolume.getAbsolutePath())) {
                    return true;
                }
            }
        }
        return false;
    }

    private Boolean isPrimary(StorageVolume storageVolume) {
        Boolean primary = androidStorageRepository.isPrimary(storageVolume);
        if (primary != null) {
            return primary;
        }
        String storageVolumePath = getStorageVolumePath(storageVolume);
        if (storageVolumePath == null) {
            return false;
        }
        return isPrimaryLegacy(new File(storageVolumePath));
    }

    private boolean isPrimaryLegacy(File storageVolume) {
        if (!isStringEmpty(SECONDARY_STORAGES)) {
            for (String path : SECONDARY_STORAGES.split(File.pathSeparator)) {
                if (!isStringEmpty(path) && storageVolume.getAbsolutePath().startsWith(path)) {
                    return false;
                }
            }
        }

        File externalStorageDirectory = Environment.getExternalStorageDirectory();
        return externalStorageDirectory.getAbsolutePath().startsWith(storageVolume.getAbsolutePath());
    }

    private Boolean isRemovable(StorageVolume storageVolume) {
        Boolean removable = androidStorageRepository.isRemovable(storageVolume);
        if (removable != null) {
            return removable;
        }
        String storageVolumePath = getStorageVolumePath(storageVolume);
        if (storageVolumePath == null) {
            return false;
        }
        return isRemovableLegacy(new File(storageVolumePath));
    }

    private boolean isRemovableLegacy(File storageVolume) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return Environment.isExternalStorageRemovable(storageVolume);
        }
        if (Environment.getExternalStorageDirectory().getAbsolutePath().startsWith(storageVolume.getAbsolutePath())) {
            return Environment.isExternalStorageRemovable();
        }

        // treat any undetectable storage as removable to be safe
        return true;
    }

    private String getUserLabel(StorageVolume storageVolume) {
        return androidStorageRepository.getUserLabel(storageVolume);
    }

    private long getTotalSpaceLegacy(StatFs statFs) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            return statFs.getTotalBytes();
        }
        return ((long) statFs.getBlockSize()) * statFs.getBlockCount();
    }

    private long getOccupiedSpaceLegacy(StatFs statFs) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            return statFs.getTotalBytes() - statFs.getAvailableBytes();
        }
        return getTotalSpaceLegacy(statFs) - ((long) statFs.getBlockSize()) * statFs.getAvailableBlocks();
    }

    private boolean isStringEmpty(String value) {
        return value == null || value.length() == 0;
    }

}
