package com.chiefcomma.app.storage;

import android.content.SharedPreferences;

import com.chiefcomma.app.config.qualifier.StorageVolumeSharedPreferences;
import com.chiefcomma.app.storage.repository.WfmStorageRepository;
import com.chiefcomma.app.storage.repository.WfmStorageVolume;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class WfmStorageService {

    private final WfmStorageRepository wfmStorageRepository;
    private final SharedPreferences storageVolumeSharedPreferences;

    @Inject
    public WfmStorageService(
            WfmStorageRepository wfmStorageRepository,
            @StorageVolumeSharedPreferences SharedPreferences storageVolumeSharedPreferences
    ) {
        this.wfmStorageRepository = wfmStorageRepository;
        this.storageVolumeSharedPreferences = storageVolumeSharedPreferences;
    }

    public List<WfmStorageVolume> getEnabledStorageVolumes() {
        List<WfmStorageVolume> result = new ArrayList<>();
        for (WfmStorageVolume wfmStorageVolume : wfmStorageRepository.storageList()) {
            if (isStorageVolumeEnabled(wfmStorageVolume)) {
                result.add(wfmStorageVolume);
            }
        }
        return result;
    }

    public void enableInternalStorageForFirstRun() {
        for (WfmStorageVolume wfmStorageVolume : wfmStorageRepository.storageList()) {
            if (wfmStorageVolume.getStorageType() == StorageType.INTERNAL) {
                setStorageVolumeState(wfmStorageVolume, true);
                return;
            }
        }
    }

    public void disableStorageVolumesNotFoundInSystem() {
        List<WfmStorageVolume> wfmStorageVolumes = wfmStorageRepository.storageList();
        for (Map.Entry<String, ?> entry : storageVolumeSharedPreferences.getAll().entrySet()) {
            String storageVolumePath = entry.getKey();
            if (!storageVolumeExists(wfmStorageVolumes, storageVolumePath)) {
                storageVolumeSharedPreferences.edit()
                        .remove(storageVolumePath)
                        .apply();
            }
        }
    }

    private boolean storageVolumeExists(
            List<WfmStorageVolume> wfmStorageVolumes,
            String storageVolumePath
    ) {
        for (WfmStorageVolume wfmStorageVolume : wfmStorageVolumes) {
            if (storageVolumePath.equals(wfmStorageVolume.getAbsolutePath())) {
                return true;
            }
        }
        return false;
    }

    public boolean isStorageVolumeEnabled(WfmStorageVolume wfmStorageVolume) {
        return storageVolumeSharedPreferences
                .getBoolean(wfmStorageVolume.getAbsolutePath(), false);
    }

    public void setStorageVolumeState(WfmStorageVolume wfmStorageVolume, boolean isEnabled) {
        setStorageVolumeState(wfmStorageVolume.getAbsolutePath(), isEnabled);
    }

    public void setStorageVolumeState(String storageVolumeAbsolutePath, boolean isEnabled) {
        storageVolumeSharedPreferences.edit()
                .putBoolean(storageVolumeAbsolutePath, isEnabled)
                .apply();
    }
}
