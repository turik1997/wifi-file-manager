package com.chiefcomma.app.gui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.storage.StorageVolume;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.chiefcomma.app.R;
import com.chiefcomma.app.SharedMemory;
import com.chiefcomma.app.storage.repository.WfmStorageVolume;
import com.chiefcomma.app.utils.IntentHelper;

import java.io.File;
import java.util.Random;

/**
 * Created by Turkhan Badalov on 11/5/17.
 */

public class StorageAccessFrameworkRationaleDialog extends DialogFragment {

    private final Activity activity;
    private final WfmStorageVolume wfmStorageVolume;

    public StorageAccessFrameworkRationaleDialog(
            Activity activity,
            WfmStorageVolume wfmStorageVolume
    ) {
        this.activity = activity;
        this.wfmStorageVolume = wfmStorageVolume;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getContext());
        configureDialog(dialog);
        return dialog;
    }

    private void configureDialog(Dialog dialog) {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.saf_rationale_dialog_layout);

        configureInstructionsWebView(dialog);
        configureButtons(dialog);
        makeDialogFullScreen(dialog);
    }

    private void configureInstructionsWebView(Dialog dialog) {
        WebView webView = (WebView) dialog.findViewById(R.id.web_view_saf);
        // according to this answer to speed up the webview
        // source: https://stackoverflow.com/a/32304329/6657837
        if (Build.VERSION.SDK_INT >= 19) {
            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }
        else {
            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        webView.loadUrl("file:///android_asset/saf_instructions.html");
    }

    private void configureButtons(Dialog dialog) {
        configureBackButton(dialog);
        configureCancelButton(dialog);
        configureContinueButton(dialog);
    }

    private static void configureBackButton(Dialog dialog) {
        ImageView buttonBack = (ImageView) dialog.findViewById(R.id.btn_close_saf_dialog);
        buttonBack.setOnClickListener(view -> dialog.dismiss());
    }

    private void configureCancelButton(Dialog dialog) {
        Button buttonCancel = (Button) dialog.findViewById(R.id.btn_saf_cancel);
        buttonCancel.setOnClickListener(btn -> dialog.dismiss());
    }

    private void configureContinueButton(Dialog dialog) {
        Button buttonContinue = (Button) dialog.findViewById(R.id.btn_saf_continue);
        buttonContinue.setOnClickListener(btn -> {
            rememberRequestCodeAndOpenUiPicker(dialog);
            dialog.dismiss();
        });
    }

    private void makeDialogFullScreen(Dialog dialog) {
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.setOnShowListener(d -> dialog.getWindow().setAttributes(lp));
    }

    private void rememberRequestCodeAndOpenUiPicker(Dialog dialog) {
        final Intent openDocumentTreeIntent = buildIntentForUiPicker();
        int requestCode = prepareRequestCode();
        activity.startActivityForResult(openDocumentTreeIntent, requestCode);
    }

    private int prepareRequestCode() {
        int requestCode = getRandomRequestCode();
        SharedMemory.requestCodes.put(requestCode, new File(wfmStorageVolume.getAbsolutePath()));
        return requestCode;
    }

    private Intent buildIntentForUiPicker() {
        StorageVolume storageVolume = wfmStorageVolume.getStorageVolume();
        if (storageVolume != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                Intent openDocumentTreeIntent = storageVolume.createOpenDocumentTreeIntent();
                return IntentHelper.openUiPickerToSelectRightStorage(openDocumentTreeIntent);
            }
        }
        return IntentHelper.openUiPickerToSelectRightStorage(new File(wfmStorageVolume.getAbsolutePath()));
    }

    private int getRandomRequestCode() {
        return Math.max(
                1,
                Math.abs(new Random(System.currentTimeMillis()).nextInt())
        );
    }
}
