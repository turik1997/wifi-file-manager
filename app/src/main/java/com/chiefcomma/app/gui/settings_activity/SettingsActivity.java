package com.chiefcomma.app.gui.settings_activity;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import com.chiefcomma.app.R;
import com.chiefcomma.app.gui.dialogs.RestartDialogBuilder;
import com.chiefcomma.app.gui.settings_activity.fragments.SettingsFragment;

import dagger.hilt.android.AndroidEntryPoint;

/**
 * Created by Turkhan Badalov on 9/15/17.
 */

@AndroidEntryPoint
public class SettingsActivity extends AppCompatActivity {

    public static String PORT_KEY = "pref_key_port";
    public static String PASSWORD_KEY = "pref_key_password";

    private final SettingsFragment settingsFragment = new SettingsFragment();
    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_layout);
        findViews();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        //getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        RestartDialogBuilder.setContext(this);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.settings_frame, settingsFragment)
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // https://stackoverflow.com/questions/62856709/back-button-not-working-on-preferences-multiple-screens
        if (item.getItemId() == android.R.id.home) {
            // https://stackoverflow.com/questions/72634225/onbackpressed-is-deprecated-what-is-the-alternative
            getOnBackPressedDispatcher().onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void findViews(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
    }
}
