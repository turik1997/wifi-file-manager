package com.chiefcomma.app.gui.main_activity;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.widget.ToggleButton;

import com.chiefcomma.app.androidservice.MainServiceBinder;
import com.chiefcomma.app.androidservice.interfaces.ServiceRunningCallback;

import javax.inject.Inject;

import dagger.hilt.android.qualifiers.ActivityContext;
import dagger.hilt.android.scopes.ActivityScoped;

@ActivityScoped
public class MainServiceConnection implements ServiceConnection {

    private final Activity activity;
    private volatile ToggleButton toggleButton;

    @Inject
    public MainServiceConnection(@ActivityContext Context activityContext) {
        this.activity = (Activity) activityContext;
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        MainServiceBinder serviceBinder = (MainServiceBinder) service;
        serviceBinder.setServiceRunningCallback(new ServiceRunningCallback() {
            @Override
            public void onStop() {
                activity.runOnUiThread(() -> {
                    if (toggleButton.isChecked()) {
                        toggleButton.performClick();
                    }
                });
            }
        });
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
    }

    public void setToggleButton(ToggleButton toggleButton) {
        this.toggleButton = toggleButton;
    }
}
