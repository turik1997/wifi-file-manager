package com.chiefcomma.app.gui.main_activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chiefcomma.app.R;
import com.chiefcomma.app.network.NetworkInterfaceType;
import com.chiefcomma.app.network.WfmNetworkInterface;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.qualifiers.ApplicationContext;
import dagger.hilt.android.scopes.FragmentScoped;

@FragmentScoped
public class NetworkListAdapter extends RecyclerView.Adapter<NetworkListAdapter.ViewHolder> {

    private final LayoutInflater layoutInflater;
    private final Context applicationContext;
    private final MainActivityPresenter mainActivityPresenter;
    private volatile List<WfmNetworkInterface> networkInterfaces = new ArrayList<>();

    @Inject
    public NetworkListAdapter(
            @ApplicationContext Context applicationContext,
            LayoutInflater layoutInflater,
            MainActivityPresenter mainActivityPresenter
    ) {
        this.layoutInflater = layoutInflater;
        this.mainActivityPresenter = mainActivityPresenter;
        this.applicationContext = applicationContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = layoutInflater.inflate(R.layout.network_item_layout, parent, false);
        return buildViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        WfmNetworkInterface wfmNetworkInterface = networkInterfaces.get(position);
        configureBottomBorder(holder, position);
        configureContent(holder, wfmNetworkInterface);
    }

    @Override
    public int getItemCount() {
        return networkInterfaces.size();
    }

    public void setNetworkInterfaces(List<WfmNetworkInterface> networkInterfaces) {
        this.networkInterfaces = networkInterfaces;
    }

    @NonNull
    private ViewHolder buildViewHolder(View rootView) {
        ViewHolder viewHolder = new ViewHolder(rootView);
        rootView.setOnClickListener(clickedView -> mainActivityPresenter.displayNetworkInterfaceUrl(
                getItem(viewHolder.getBindingAdapterPosition()).getIpAddress()
        ));
        return viewHolder;
    }

    private WfmNetworkInterface getItem(int itemPosition) {
        return networkInterfaces.get(itemPosition);
    }

    private void configureBottomBorder(@NonNull ViewHolder holder, int position) {
        holder.borderBottom.setVisibility(View.GONE);
        if (networkInterfaces.size() == 1 || networkInterfaces.size() > 1 && position < networkInterfaces.size() - 1) {
            holder.borderBottom.setVisibility(View.VISIBLE);
        }
    }

    private void configureContent(
            @NonNull ViewHolder holder,
            WfmNetworkInterface wfmNetworkInterface
    ) {
        holder.txtNetworkInterfaceDescription.setText(getDescription(wfmNetworkInterface));
        holder.txtIpAddress.setText(wfmNetworkInterface.getIpAddress());
        holder.imgNetworkIcon.setVisibility(View.VISIBLE);
        holder.imgNetworkIcon.setImageResource(getDrawable(wfmNetworkInterface));
    }

    private String getDescription(WfmNetworkInterface wfmNetworkInterface) {
        String networkInterfaceTypeName = getNetworkInterfaceTypeName(wfmNetworkInterface);
        return networkInterfaceTypeName + " (" + wfmNetworkInterface.getName() + ")";
    }

    private String getNetworkInterfaceTypeName(WfmNetworkInterface wfmNetworkInterface) {
        int stringResource = getResourceStringForNetworkInterfaceType(wfmNetworkInterface.getNetworkInterfaceType());
        return applicationContext.getResources().getString(stringResource);
    }

    private int getResourceStringForNetworkInterfaceType(NetworkInterfaceType networkInterfaceType) {
        switch (networkInterfaceType) {
            case WIFI:
                return R.string.wifi;
            case WIFI_HOTSPOT:
                return R.string.wifi_hotspot;
            case USB_TETHERING:
                return R.string.usb_tethering;
            case BLUETOOTH_TETHERING:
                return R.string.bluetooth_tethering;
            case LOOPBACK:
                return R.string.localhost;
            case OTHER:
            default:
                return R.string.other_network;
        }
    }

    private int getDrawable(WfmNetworkInterface wfmNetworkInterface) {
        switch (wfmNetworkInterface.getNetworkInterfaceType()) {
            case WIFI:
                return R.drawable.wifi;
            case WIFI_HOTSPOT:
                return R.drawable.wifi_tethering;
            case USB_TETHERING:
                return R.drawable.usb;
            case BLUETOOTH_TETHERING:
                return R.drawable.bluetooth_searching;
            default:
                return R.drawable.phonelink_ring;
        }
    }

    static final class ViewHolder
            extends RecyclerView.ViewHolder {
        ImageView imgNetworkIcon;
        TextView txtNetworkInterfaceDescription;
        TextView txtIpAddress;
        View borderBottom;

        ViewHolder(View itemView) {
            super(itemView);
            txtNetworkInterfaceDescription = itemView.findViewById(R.id.txt_network_name);
            txtIpAddress = itemView.findViewById(R.id.txt_network_address);
            borderBottom = itemView.findViewById(R.id.list_item_bottom_border);
            imgNetworkIcon = itemView.findViewById(R.id.img_network_icon);
        }
    }
}
