package com.chiefcomma.app.gui.main_activity;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.chiefcomma.app.R;
import com.chiefcomma.app.storage.WfmStorageService;
import com.chiefcomma.app.storage.repository.WfmStorageVolume;
import com.chiefcomma.app.utils.FileUtil;
import com.chiefcomma.app.utils.storage_access_framework.SafUriManager;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class StorageListAdapter extends RecyclerView.Adapter<StorageListAdapter.ViewHolder> {

    private final LayoutInflater layoutInflater;;
    private final WfmStorageService wfmStorageService;
    private final SafUriManager uriManager;
    private AppCompatActivity activity;
    private volatile List<WfmStorageVolume> storageVolumes = new ArrayList<>();

    @Inject
    public StorageListAdapter(
            LayoutInflater layoutInflater,
            WfmStorageService wfmStorageService,
            SafUriManager uriManager
    ) {
        this.layoutInflater = layoutInflater;
        this.wfmStorageService = wfmStorageService;
        this.uriManager = uriManager;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.storage_item_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        WfmStorageVolume wfmStorageVolume = storageVolumes.get(position);
        configureBottomBorder(holder, position);
        configureContent(holder, position, wfmStorageVolume);
        holder.switchStorageSharing.setOnClickListener(new StorageVolumeToggleButtonListener(
                activity,
                wfmStorageVolume,
                uriManager,
                wfmStorageService
        ));
    }

    @Override
    public int getItemCount() {
        return storageVolumes.size();
    }

    public void setStorageVolumes(List<WfmStorageVolume> storageVolumes) {
        this.storageVolumes = storageVolumes;
    }

    public void setActivity(AppCompatActivity activity) {
        this.activity = activity;
    }

    private void configureBottomBorder(@NonNull ViewHolder holder, int position) {
        holder.borderBottom.setVisibility(View.GONE);
        if (storageVolumes.size() > 1 && position < storageVolumes.size() - 1) {
            holder.borderBottom.setVisibility(View.VISIBLE);
        }
    }

    private void configureContent(@NonNull ViewHolder holder, int position, WfmStorageVolume wfmStorageVolume) {
        holder.switchStorageSharing.setChecked(wfmStorageService.isStorageVolumeEnabled(wfmStorageVolume));
        holder.txtStorageDescription.setText(getDescription(position));
        setStorageSpaceUsageInformation(holder, wfmStorageVolume);
        holder.imgStorageIcon.setImageResource(getDrawable(position));
    }

    @SuppressLint("SetTextI18n")
    private void setStorageSpaceUsageInformation(
            @NonNull ViewHolder holder,
            WfmStorageVolume wfmStorageVolume
    ) {
        if (wfmStorageVolume.getTotalSpace() == null || wfmStorageVolume.getOccupiedSpace() == null) {
            holder.txtStorageSize.setVisibility(View.INVISIBLE);
        } else {
            String occupiedSpace = FileUtil.byteCountToDisplaySize(BigInteger.valueOf(wfmStorageVolume.getOccupiedSpace()));
            String totalSpace = FileUtil.byteCountToDisplaySize(BigInteger.valueOf(wfmStorageVolume.getTotalSpace()));
            holder.txtStorageSize.setText(occupiedSpace + " / " + totalSpace);
            holder.txtStorageSize.setVisibility(View.VISIBLE);
        }
    }

    private String getDescription(int position) {
        WfmStorageVolume currentStorageVolume = storageVolumes.get(position);
        int sameTypeCount = 0;
        for (WfmStorageVolume wfmStorageVolume : storageVolumes) {
            if (wfmStorageVolume != currentStorageVolume && wfmStorageVolume.getStorageType() == currentStorageVolume.getStorageType()) {
                sameTypeCount++;
            }
        }
        if (sameTypeCount > 0 && currentStorageVolume.getUserLabel() != null) {
            return currentStorageVolume.getDescription() + ": " + currentStorageVolume.getUserLabel();
        }
        return currentStorageVolume.getDescription();
    }

    private int getDrawable(int position) {
        WfmStorageVolume wfmStorageVolume = storageVolumes.get(position);
        switch (wfmStorageVolume.getStorageType()) {
            case INTERNAL:
                return R.drawable.ic_phone_android_black_24dp;
            case SD_CARD:
                return R.drawable.ic_sd_card_black_24dp;
            case USB:
                return R.drawable.ic_usb_black_24dp;
            default:
                return R.drawable.ic_unknown_storage_black_24dp;
        }
    }

    static final class ViewHolder
            extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        ImageView imgStorageIcon;
        TextView txtStorageDescription;
        TextView txtStorageSize;
        View borderBottom;
        Switch switchStorageSharing;

        ViewHolder(View itemView) {
            super(itemView);
            txtStorageDescription = itemView.findViewById(R.id.txt_storage_description);
            txtStorageSize = itemView.findViewById(R.id.txt_storage_size);
            borderBottom = itemView.findViewById(R.id.list_item_bottom_border);
            imgStorageIcon = itemView.findViewById(R.id.storage_icon);
            switchStorageSharing = itemView.findViewById(R.id.switch_storage_sharing);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switchStorageSharing.performClick();
        }
    }
}
