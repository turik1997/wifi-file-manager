package com.chiefcomma.app.gui.main_activity;

import android.os.Build;
import android.view.View;
import android.widget.Switch;

import androidx.appcompat.app.AppCompatActivity;

import com.chiefcomma.app.storage.WfmStorageService;
import com.chiefcomma.app.storage.repository.WfmStorageVolume;
import com.chiefcomma.app.utils.DialogHelper;
import com.chiefcomma.app.utils.storage_access_framework.SafUriManager;

import java.io.File;

public class StorageVolumeToggleButtonListener implements View.OnClickListener {

    private final AppCompatActivity activity;
    private final WfmStorageVolume wfmStorageVolume;
    private final SafUriManager uriManager;
    private final WfmStorageService wfmStorageService;

    public StorageVolumeToggleButtonListener(
            AppCompatActivity activity,
            WfmStorageVolume wfmStorageVolume,
            SafUriManager uriManager,
            WfmStorageService wfmStorageService
    ) {
        this.activity = activity;
        this.wfmStorageVolume = wfmStorageVolume;
        this.uriManager = uriManager;
        this.wfmStorageService = wfmStorageService;
    }

    @Override
    public void onClick(View v) {
        Switch s = (Switch) v;
        boolean isStorageVolumeEnabled = s.isChecked();
        if (isStorageVolumeEnabled && safAccessNeededForStorageVolume()) {
            DialogHelper.showStorageAccessFrameworkRationaleDialog(
                    activity,
                    wfmStorageVolume
            );
            s.setChecked(false);
            return;
        }
        wfmStorageService.setStorageVolumeState(wfmStorageVolume, isStorageVolumeEnabled);
    }

    private boolean safAccessNeededForStorageVolume() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT
                && wfmStorageVolume.getRemovable()
                && enablingFirstTime();
    }

    private boolean enablingFirstTime() {
        return uriManager.getPreviouslySavedTreeUri(new File(wfmStorageVolume.getAbsolutePath())) == null;
    }
}
