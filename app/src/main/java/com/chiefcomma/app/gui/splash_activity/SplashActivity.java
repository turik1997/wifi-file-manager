package com.chiefcomma.app.gui.splash_activity;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.chiefcomma.app.R;
import com.chiefcomma.app.gui.main_activity.MainActivity;
import com.chiefcomma.app.storage.WfmStorageService;
import com.chiefcomma.app.utils.IntentHelper;
import com.chiefcomma.app.utils.PermissionHelper;
import com.chiefcomma.app.utils.storage_access_framework.SafUriManager;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;

/**
 * Created by Turkhan Badalov on 11/5/17.
 */

@AndroidEntryPoint
public class SplashActivity extends AppCompatActivity {

    private static final String FIRST_RUN = "FIRST_RUN";

    @Inject
    WfmStorageService wfmStorageService;
    @Inject
    SharedPreferences defaultSharedPreferences;
    @Inject
    SafUriManager safUriManager;

    private final ActivityResultLauncher<String> requestPermissionLauncher = registerForActivityResult(
            new ActivityResultContracts.RequestPermission(),
            isGranted -> {
                if (isGranted) {
                    onPermissionGranted();
                } else {
                    onPermissionDenied();
                }
            }
    );
    private final PermissionHelper permissionHelper = new PermissionHelper(
            this::onPermissionGranted,
            this::onPermissionDenied,
            this.requestPermissionLauncher,
            this
    );
    private boolean wasDenied = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new Thread(safUriManager::removeTempDirs).start();
        new Thread(() -> {
            wfmStorageService.disableStorageVolumesNotFoundInSystem();
            enableInternalStorageVolumeForFirstRun();
        }).start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // switch to MainActivity right after the user enables full storage access and comes back
        this.permissionHelper.reset();
        this.permissionHelper.checkNecessaryPermissions();
    }

    private void enableInternalStorageVolumeForFirstRun() {
        if (isFirstTimeRunning()) {
            wfmStorageService.enableInternalStorageForFirstRun();
            defaultSharedPreferences.edit()
                    .putBoolean(FIRST_RUN, false)
                    .apply();
        }
    }

    private boolean isFirstTimeRunning() {
        return defaultSharedPreferences.getBoolean(FIRST_RUN, true);
    }

    private void openMainActivity() {
        Intent mainActivityIntent = new Intent(this, MainActivity.class);
        mainActivityIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        mainActivityIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(mainActivityIntent);
        finish();
    }

    private void initPermissionNotGrantedLayout() {
        setContentView(R.layout.storage_permission_not_granted);
        findViewById(R.id.btn_open_settings).setOnClickListener(btn -> {
            List<String> permissionList = Arrays.asList(
                    Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION,
                    Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION
            );
            for (String permission : permissionList) {
                if (IntentHelper.tryFullStorageAccessSettingsActivity(this, permission)) {
                    return;
                }
            }
            Intent intent = buildActionIntent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        });
    }

    private Intent buildActionIntent(String action) {
        return new Intent(action, Uri.fromParts("package", getPackageName(), null));
    }

    private void onPermissionGranted() {
        wasDenied = false;
        openMainActivity();
    }

    private void onPermissionDenied() {
        wasDenied = true;
        initPermissionNotGrantedLayout();
    }
}
