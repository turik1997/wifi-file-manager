package com.chiefcomma.app.gui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chiefcomma.app.R;
import com.chiefcomma.app.gui.main_activity.StorageListAdapter;
import com.chiefcomma.app.storage.repository.WfmStorageRepository;
import com.chiefcomma.app.storage.repository.WfmStorageVolume;

import java.util.List;

public class ChooseStorageDialog extends Dialog {

    private final WfmStorageRepository wfmStorageRepository;
    private final StorageListAdapter storageListAdapter;
    private final Activity activity;
    private View loadingGroup;
    private RecyclerView recyclerView;
    private TextView txtNoStorageFound;
    private TextView txtTitle;

    public ChooseStorageDialog(
            @NonNull Activity activity,
            WfmStorageRepository wfmStorageRepository,
            StorageListAdapter storageListAdapter
    ) {
        super(activity);
        this.activity = activity;
        this.wfmStorageRepository = wfmStorageRepository;
        this.storageListAdapter = storageListAdapter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); // should be called before changing any view
        setContentView(R.layout.choose_storage_layout);
        findViews();
        configViews();
        new Thread(() -> {
            List<WfmStorageVolume> wfmStorageVolumes = wfmStorageRepository.storageList();
            if (wfmStorageVolumes.isEmpty()) {
                showNoStorageText();
            } else {
                storageListAdapter.setStorageVolumes(wfmStorageVolumes);
                showStorageListToChoose();
            }
        }).start();
    }

    private void findViews() {
        txtTitle = findViewById(R.id.txt_title_hint);
        loadingGroup = findViewById(R.id.loading_panel);
        recyclerView = findViewById(R.id.storage_list);
        txtNoStorageFound = findViewById(R.id.no_storage_volume_info);
    }

    private void configViews() {
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        recyclerView.setAdapter(storageListAdapter);
    }

    private void showStorageListToChoose() {
        activity.runOnUiThread(() -> {
            storageListAdapter.notifyDataSetChanged();
            loadingGroup.setVisibility(View.GONE);
            txtNoStorageFound.setVisibility(View.GONE);
            txtTitle.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.VISIBLE);
        });
    }

    private void showNoStorageText() {
        activity.runOnUiThread(() -> {
            loadingGroup.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);
            txtTitle.setVisibility(View.VISIBLE);
            txtNoStorageFound.setVisibility(View.VISIBLE);
        });
    }
}
