package com.chiefcomma.app.gui.settings_activity.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;
import androidx.annotation.Nullable;

import com.chiefcomma.app.R;
import com.chiefcomma.app.androidservice.interfaces.PreferenceFinder;
import com.chiefcomma.app.gui.settings_activity.listeners.ConfigsChangedListener;

/**
 * Created by Turkhan Badalov on 9/15/17.
 */

public class SettingsFragment extends PreferenceFragmentCompat implements PreferenceFinder {

    private SharedPreferences preferences;
    private ConfigsChangedListener configsChangedListener;


    public SettingsFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        configsChangedListener = new ConfigsChangedListener(null, this, getResources());
    }

    @Override
    public void onCreatePreferences(@Nullable Bundle savedInstanceState, @Nullable String rootKey) {
        setPreferencesFromResource(R.xml.preferences, rootKey);
    }

    @Override
    public void onResume() {
        super.onResume();
        preferences.registerOnSharedPreferenceChangeListener(configsChangedListener);
    }

    @Override
    public void onPause() {
        super.onPause();
        preferences.unregisterOnSharedPreferenceChangeListener(configsChangedListener);
    }

    @Override
    public Preference findPreference(CharSequence key) {
        return getPreferenceManager().findPreference(key);
    }
}
