package com.chiefcomma.app.gui.dialogs;

import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chiefcomma.app.R;
import com.chiefcomma.app.gui.main_activity.NetworkListAdapter;
import com.chiefcomma.app.network.NetworkInterfaceRepository;
import com.chiefcomma.app.network.NetworkService;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class MyModalBottomSheetDialog extends BottomSheetDialogFragment {

    private View loadingPanel;
    private RecyclerView recyclerView;
    @Inject
    NetworkListAdapter networkListAdapter;
    @Inject
    NetworkService networkService;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.bototm_sheet_layout, container, false);
        findViews(rootView);
        configureNetworkList(recyclerView);
        return rootView;
    }

    // Had to use onViewCreated to configure the behavior of the BottomSheetDialog
    // https://github.com/material-components/material-components-android/issues/3018
    // Solution: https://stackoverflow.com/a/76283458/6657837
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) getDialog();
        View parentLayout = bottomSheetDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);
        ViewGroup.LayoutParams layoutParams = parentLayout.getLayoutParams();
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.width = Math.min(dpToPx(450), getWidth());
        parentLayout.setLayoutParams(layoutParams);
        BottomSheetBehavior<FrameLayout> bottomSheetDialogBehavior = bottomSheetDialog.getBehavior();
        bottomSheetDialogBehavior.setHalfExpandedRatio(0.4f);
        bottomSheetDialogBehavior.setPeekHeight(4 * getWindowHeight() / 10);
    }

    private void findViews(View rootView) {
        recyclerView = rootView.findViewById(R.id.network_list);
        loadingPanel = rootView.findViewById(R.id.loading_panel);
    }

    private void configureNetworkList(RecyclerView recyclerView) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(networkListAdapter);
        loadNetworkListData(recyclerView);
    }

    private void loadNetworkListData(RecyclerView recyclerView) {
        new Thread(() -> {
            networkListAdapter.setNetworkInterfaces(networkService.getNetworkInterfaces());
            FragmentActivity parentActivity = getActivity();
            // case when the dialog is already closed before the task finished
            if (parentActivity != null) {
                parentActivity.runOnUiThread(() -> hideLoadingShowNetworkList(recyclerView));
            }
        }).start();
    }

    private void hideLoadingShowNetworkList(RecyclerView recyclerView) {
        loadingPanel.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    private int getWindowHeight() {
        // Calculate window height for fullscreen use
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    private int getWidth() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return Math.min(dpToPx(450), displayMetrics.widthPixels);
    }

    private static int dpToPx(int dp) {
        // https://developer.android.com/guide/practices/screens_support.html#dips-pels
        float density = Resources.getSystem().getDisplayMetrics().density;
        return (int) ((dp * density) + 0.5f);
    }
}