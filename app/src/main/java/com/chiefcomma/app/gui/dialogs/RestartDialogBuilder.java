package com.chiefcomma.app.gui.dialogs;

import android.app.AlertDialog;
import android.content.Context;

import java.lang.ref.WeakReference;

/**
 * Created by Hp on 9/19/2017.
 */

public class RestartDialogBuilder {

    private static AlertDialog.Builder builder;
    private static WeakReference<Context> contextWeakReference;

    public static void setContext(Context context) {
        contextWeakReference = new WeakReference<Context>(context);
    }

    public static AlertDialog getInstance(int port) {

        if ( builder == null )
        {
            builder = new AlertDialog.Builder(contextWeakReference.get())
                    .setTitle("Restart Required")
                    .setPositiveButton("Restart Now", null)
                    .setNegativeButton("Restart Later", null);
        }

        return builder.setMessage(port + " ahaha").create();
    }
}
