package com.chiefcomma.app.gui.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.os.BuildCompat;
import androidx.multidex.BuildConfig;

import android.text.Spanned;
import android.text.util.Linkify;
import android.widget.ImageView;
import android.widget.TextView;

import com.chiefcomma.app.R;

/**
 * Created by Turkhan Badalov on 11/5/17.
 */

public class AboutDialog extends Dialog {

    private ImageView imgLogo;
    private TextView txtAppVersion;
    private TextView txtContact;
    private Spanned contactContent;

    public AboutDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        findViews();
        imgLogo.setImageBitmap(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.wfm_logo_splash));
        String appVersion = "";
        try {
            PackageInfo packageInfo = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0);
            appVersion = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
        }
        if (appVersion != null && !appVersion.isEmpty()) {
            txtAppVersion.setText("v" + appVersion);
        }

        txtContact.setText(contactContent);
        txtContact.setLinkTextColor(Color.BLUE);
        Linkify.addLinks(txtContact, Linkify.ALL);
    }

    public void setContactContent(Spanned contactContent) {
        this.contactContent = contactContent;
    }

    private void findViews(){
        imgLogo = (ImageView) findViewById(R.id.img_logo);
        txtAppVersion = (TextView) findViewById(R.id.txt_app_version);
        txtContact = (TextView) findViewById(R.id.txt_contact);
    }
}
