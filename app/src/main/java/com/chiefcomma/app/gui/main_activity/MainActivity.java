package com.chiefcomma.app.gui.main_activity;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;

import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ShareCompat;
import androidx.preference.PreferenceManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.chiefcomma.app.R;
import com.chiefcomma.app.SharedMemory;
import com.chiefcomma.app.gui.dialogs.MyModalBottomSheetDialog;
import com.chiefcomma.app.storage.WfmStorageService;
import com.chiefcomma.app.storage.repository.WfmStorageRepository;
import com.chiefcomma.app.androidservice.MainService;
import com.chiefcomma.app.utils.DialogHelper;
import com.chiefcomma.app.utils.IntentHelper;
import com.chiefcomma.app.utils.storage_access_framework.SafUriManager;
import com.chiefcomma.wfm.utils.MimeTypes;

import java.io.File;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import dev.doubledot.doki.ui.DokiActivity;

@AndroidEntryPoint
public class MainActivity extends AppCompatActivity {


    public final static String IS_FROM_NOTIFICATION = "FROM_NOTIF";
    volatile public static boolean VISIBLE = false;
    private boolean isSharingDone = true;

    private ToggleButton btnStart;
    private Toolbar toolbar;
    private TextView txtAddress;
    private AppCompatImageView btnShareAddress;

    @Inject
    WfmStorageRepository wfmStorageRepository;
    @Inject
    WfmStorageService wfmStorageService;
    @Inject
    StorageListAdapter storageListAdapter;
    @Inject
    SafUriManager safUriManager;
    @Inject
    MainServiceConnection mainServiceConnection;
    @Inject
    BtnStartListener btnStartListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // To prevent Binary XML file: Error inflating class android.widget.ImageView
        // Caused by: java.lang.reflect.InvocationTargetException
        // https://stackoverflow.com/a/47526771/6657837
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        setContentView(R.layout.activity_main);
        PreferenceManager.setDefaultValues(getApplicationContext(), R.xml.preferences, false);
        findViews();
        configViews();
        configToolbar();
        storageListAdapter.setActivity(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.POST_NOTIFICATIONS}, 100);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel("wfm_channel", "Wifi File Manager");
        }
    }

    private void configToolbar() {
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.aza, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && isSafRequest(requestCode)) {
            Uri uri = getStorageVolumeTreeUriFromResult(resultCode, data);
            if (uri == null) {
                Toast.makeText(this, "Couldn't get access to the storage", Toast.LENGTH_LONG).show();
                return;
            }
            File file = getFileThatUriWasRequestedFor(requestCode);
            processRetrievedTreeUri(uri, file);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Nullable
    private Uri getStorageVolumeTreeUriFromResult(int resultCode, @Nullable Intent data) {
        ActivityResultContracts.OpenDocumentTree openDocumentTree = new ActivityResultContracts.OpenDocumentTree();
        return openDocumentTree.parseResult(resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.app_not_working:
                DokiActivity.Companion.start(this);
                return true;
            case R.id.item_choose_storage:
                DialogHelper.showChooseStorageDialog(this, wfmStorageRepository, storageListAdapter);
                return true;
            case R.id.item_settings: {
                IntentHelper.startSettingsActivity(this);
                return true;
            }
            case R.id.about: {
                DialogHelper.showAboutDialog(this);
                return true;
            }
        }

        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        VISIBLE = true;
        isSharingDone = true;
        if (MainService.IS_RUNNING) {
            btnStart.setChecked(false);
            btnStart.performClick();
        } else if (btnStart.isChecked()) {
            btnStart.performClick();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        VISIBLE = false;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void processRetrievedTreeUri(Uri uri, File file) {
        getApplicationContext().getContentResolver().takePersistableUriPermission(
                uri,
                Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION
        );
        if (isCorrectStorageSelected(uri, file)) {
            safUriManager.storeUriPermissionForVolume(file, uri);
            enableStorageVolume(file);
            Toast.makeText(this, "Successfully granted the access", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Please select a correct storage", Toast.LENGTH_LONG).show();
        }
    }

    private boolean isSafRequest(int requestCode) {
        return SharedMemory.requestCodes.containsKey(requestCode);
    }

    private File getFileThatUriWasRequestedFor(int requestCode) {
        return SharedMemory.requestCodes.get(requestCode);
    }

    private void enableStorageVolume(File file) {
        wfmStorageService.setStorageVolumeState(file.getAbsolutePath(), true);
        storageListAdapter.setStorageVolumes(wfmStorageRepository.storageList());
        storageListAdapter.notifyDataSetChanged();
    }

    private boolean isCorrectStorageSelected(Uri uri, File file) {
        return safUriManager.treeUriPointsToStorageVolume(file, uri);
    }

    private void configViews() {
        mainServiceConnection.setToggleButton(btnStart);
        btnStart.setOnClickListener(btnStartListener);
        txtAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyModalBottomSheetDialog myModalBottomSheetDialog = new MyModalBottomSheetDialog();
                myModalBottomSheetDialog.show(
                        getSupportFragmentManager(),
                        "some_tag"
                );
            }
        });
        btnShareAddress.setOnClickListener(view -> {
            if (isSharingDone) {
                isSharingDone = false;
                new ShareCompat.IntentBuilder(this)
                        .setType(MimeTypes.PLAIN_TEXT)
                        .setChooserTitle(R.string.share_url)
                        .setText(txtAddress.getText().toString())
                        .startChooser();
            }
        });
    }

    private void findViews() {
        btnStart = findViewById(R.id.btn_start);
        toolbar = findViewById(R.id.toolbar);
        txtAddress = findViewById(R.id.txt_address);
        btnShareAddress = findViewById(R.id.btn_share_main_address);
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private void createNotificationChannel(String channelId, String channelName) {
        NotificationChannel channel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH);
        channel.setLightColor(getResources().getColor(R.color.colorPrimaryDark));
        channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        channel.enableVibration(false);
        channel.setSound(null, null);
        channel.setShowBadge(false);
        NotificationManager service = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        service.createNotificationChannel(channel);
    }
}
