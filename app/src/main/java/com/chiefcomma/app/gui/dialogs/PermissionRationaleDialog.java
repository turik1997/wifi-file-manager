package com.chiefcomma.app.gui.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

public class PermissionRationaleDialog extends AlertDialog {

    public PermissionRationaleDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setMessage("This application is a file manager. By its very nature, it needs access to your storage to let you manage your files. Otherwise it can't see your files, hence it can't display them to you. Please allow full access to your storage.");
        super.onCreate(savedInstanceState);
    }

    public final static void showRationaleDialog(
            Context context,
            DialogInterface.OnClickListener negativeButtonListener,
            DialogInterface.OnClickListener positiveButtonListener
    ) {
        PermissionRationaleDialog dialog = new PermissionRationaleDialog(context);
        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "No, thanks", negativeButtonListener);
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", positiveButtonListener);
        dialog.show();
    }
}
