package com.chiefcomma.app.gui.main_activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.view.View;
import android.widget.Checkable;
import android.widget.TextView;
import android.widget.Toast;

import com.chiefcomma.app.R;
import com.chiefcomma.app.androidservice.MainService;
import com.chiefcomma.app.androidservice.settings.ServerConfigs;
import com.chiefcomma.app.network.NetworkService;
import com.chiefcomma.app.network.WfmNetworkInterface;
import com.chiefcomma.app.utils.NetworkHelper;

import java.net.SocketException;

import static com.chiefcomma.app.androidservice.settings.ServerConfigs.DEFAULT_PORT;

import static java.lang.Integer.*;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.preference.PreferenceManager;

import javax.inject.Inject;

import dagger.hilt.android.qualifiers.ActivityContext;
import dagger.hilt.android.scopes.ActivityScoped;

/**
 * Created by Turkhan Badalov on 9/15/17.
 */
@ActivityScoped
public class BtnStartListener implements View.OnClickListener {
    private TextView txtInstruction;
    private TextView txtAddress;
    private View controlPanelWhenEnabled;
    private View controlPanelWhenDisabled;
    private final MainServiceConnection mainServiceConnection;
    private final NetworkService networkService;
    private final Context activityContext;

    @Inject
    public BtnStartListener(
            @ActivityContext Context activityContext,
            MainServiceConnection mainServiceConnection,
            NetworkService networkService
    ) {
        this.activityContext = activityContext;
        this.mainServiceConnection = mainServiceConnection;
        this.networkService = networkService;
    }

    @Override
    public void onClick(View view) {
        AppCompatActivity activity = (AppCompatActivity) activityContext;
        this.txtInstruction = activity.findViewById(R.id.txt_prompt);
        this.txtAddress = activity.findViewById(R.id.txt_address);
        this.controlPanelWhenEnabled = activity.findViewById(R.id.txt_control_panel_enabled);
        this.controlPanelWhenDisabled = activity.findViewById(R.id.txt_control_panel_disabled);
        if (shouldStartService((Checkable) view)) {
            String ip = startService(view.getContext());
            if (ip != null) {
                txtInstruction.setVisibility(View.VISIBLE);
                txtAddress.setText(buildAddressText(view.getContext(), ip));
            } else {
                txtInstruction.setVisibility(View.INVISIBLE);
                txtAddress.setText(R.string.could_not_retrieve_an_ip_address);
            }
            controlPanelWhenDisabled.setVisibility(View.GONE);
            controlPanelWhenEnabled.setVisibility(View.VISIBLE);
            if (ContextCompat.checkSelfPermission(
                    view.getContext(),
                    Manifest.permission.POST_NOTIFICATIONS
            ) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(
                        view.getContext(),
                        view.getContext().getResources().getString(R.string.app_will_continue_running_in_the_background_when_you_leave),
                        Toast.LENGTH_LONG
                ).show();
            }
        } else {
            stopService(view.getContext());
            controlPanelWhenEnabled.setVisibility(View.GONE);
            controlPanelWhenDisabled.setVisibility(View.VISIBLE);
            if (ContextCompat.checkSelfPermission(
                    view.getContext(),
                    Manifest.permission.POST_NOTIFICATIONS
            ) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(
                        view.getContext(),
                        view.getContext().getResources().getString(R.string.the_service_has_stopped),
                        Toast.LENGTH_LONG
                ).show();
            }
        }
    }

    private String buildAddressText(Context context, String ip) {
        int port = getPort(context);
        return "http://" + ip + ":" + port;
    }

    private int getPort(Context context) {
        return parseInt(
                PreferenceManager
                        .getDefaultSharedPreferences(context)
                        .getString(
                                ServerConfigs.PORT_KEY,
                                Integer.toString(DEFAULT_PORT)
                        )
        );
    }

    private String startService(Context context) {
        Intent serviceIntent = buildServiceIntent(context);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(serviceIntent);
        } else {
            context.startService(serviceIntent);
        }
        context.bindService(new Intent(context, MainService.class), mainServiceConnection, 0);
        WfmNetworkInterface mostPreferredNetwork = networkService.getMostPreferredNetwork();
        if (mostPreferredNetwork == null) {
            Toast.makeText(context, "Something went wrong", Toast.LENGTH_LONG);
            stopService(context);
            return null;
        }
        return mostPreferredNetwork.getIpAddress();
    }

    private void stopService(Context context) {
        Intent serviceIntent = buildServiceIntent(context);
        context.unbindService(mainServiceConnection);
        context.stopService(serviceIntent);
    }

    private boolean shouldStartService(Checkable toggleButton) {
        return toggleButton.isChecked();
    }

    private Intent buildServiceIntent(Context context) {
        return new Intent(context, MainService.class);
    }
}
