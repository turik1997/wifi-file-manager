package com.chiefcomma.app.gui.settings_activity.listeners;

import android.content.SharedPreferences;
import android.content.res.Resources;
import androidx.preference.Preference;

import java.util.Formatter;

import com.chiefcomma.app.R;
import com.chiefcomma.app.androidservice.interfaces.PreferenceFinder;
import com.chiefcomma.app.androidservice.interfaces.gui.configurators.AppConfigurator;
import com.chiefcomma.app.gui.dialogs.RestartDialogBuilder;

import static com.chiefcomma.app.gui.settings_activity.SettingsActivity.PASSWORD_KEY;
import static com.chiefcomma.app.gui.settings_activity.SettingsActivity.PORT_KEY;

/**
 * Created by Hp on 9/16/2017.
 */

public class ConfigsChangedListener implements SharedPreferences.OnSharedPreferenceChangeListener {

    private final AppConfigurator configurator;
    private final PreferenceFinder preferenceFinder;
    private final Resources resources;

    public ConfigsChangedListener(
            AppConfigurator configurator,
            PreferenceFinder preferenceFinder,
            Resources resources
    ) {
        this.configurator = configurator;
        this.preferenceFinder = preferenceFinder;
        this.resources = resources;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

        if ( PASSWORD_KEY.equals(key) )
        {
            String password = sharedPreferences.getString(key, "");
            if ( configurator == null )
            {
                return;
            }

            configurator.changePassword(password);
            int statusStringId = password.length() == 0 ? R.string.not_set : R.string.is_set;
            String status = resources.getString(statusStringId);
            String summary = getSummary(status);
            Preference preference = preferenceFinder.findPreference(key);
            preference.setSummary(summary);
        }
        else
        if ( PORT_KEY.equals(key) )
        {
            int port = Integer.parseInt(sharedPreferences.getString(key, null));
            RestartDialogBuilder.getInstance(port);
        }
    }

    private String getSummaryPattern(int stringId) {
        return "(%s) " + resources.getString(stringId);
    }

    private String getSummary(String status) {
        String pattern = getSummaryPattern(R.string.password_detail);

        StringBuilder currentStatus = new StringBuilder();
        Formatter formatter = new Formatter(currentStatus);
        formatter.format(pattern, status);
        return currentStatus.toString();
    }
}
