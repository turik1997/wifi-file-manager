package com.chiefcomma.app.gui.main_activity;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.chiefcomma.app.R;
import com.chiefcomma.app.androidservice.settings.ServerConfigs;
import com.chiefcomma.app.gui.dialogs.MyModalBottomSheetDialog;

import javax.inject.Inject;

import dagger.hilt.android.qualifiers.ActivityContext;
import dagger.hilt.android.scopes.ActivityScoped;

@ActivityScoped
public class MainActivityPresenter {

    private final AppCompatActivity activity;
    private final ServerConfigs serverConfigs;
    private TextView textView;
    private TextView txtInstruction;
    private View controlPanelWhenEnabled;
    private View controlPanelWhenDisabled;

    @Inject
    public MainActivityPresenter(
            @ActivityContext Context activityContext,
            ServerConfigs serverConfigs
    ) {
        this.activity = (AppCompatActivity) activityContext;
        this.serverConfigs = serverConfigs;
    }

    public void displayServiceStarted(String ip) {
        if (ip != null) {
            getInstructionTextView().setVisibility(View.VISIBLE);
            displayNetworkInterfaceUrl(ip);
            if (ContextCompat.checkSelfPermission(
                    activity,
                    Manifest.permission.POST_NOTIFICATIONS
            ) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(
                        activity,
                        activity.getResources().getString(R.string.app_will_continue_running_in_the_background_when_you_leave),
                        Toast.LENGTH_LONG
                ).show();
            }
        } else {
            getInstructionTextView().setVisibility(View.INVISIBLE);
            getAddressTextView().setText(R.string.could_not_retrieve_an_ip_address);
        }
        getGetContolPanelWhenDisabled().setVisibility(View.GONE);
        getControlPanelWhenEnabled().setVisibility(View.VISIBLE);
    }

    public void displayServiceStopped() {
        displayDefaultScreen();
        if (ContextCompat.checkSelfPermission(
                activity,
                Manifest.permission.POST_NOTIFICATIONS
        ) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(
                    activity,
                    activity.getResources().getString(R.string.the_service_has_stopped),
                    Toast.LENGTH_LONG
            ).show();
        }
    }

    public void displayDefaultScreen() {
        getControlPanelWhenEnabled().setVisibility(View.GONE);
        getGetContolPanelWhenDisabled().setVisibility(View.VISIBLE);
    }

    public void displayNetworkInterfaceUrl(String ipAddress) {
        String address = "http://" + ipAddress + ":" + serverConfigs.getPort();
        getAddressTextView().setText(address);
        dismissBottomSheetDialog();
    }

    private void dismissBottomSheetDialog() {
        MyModalBottomSheetDialog bottomSheetFragment = getBottomSheetFragment();
        if (bottomSheetFragment != null) {
            bottomSheetFragment.dismiss();
        }
    }

    private TextView getInstructionTextView() {
        if (txtInstruction == null) {
            txtInstruction = activity.findViewById(R.id.txt_prompt);
        }
        return txtInstruction;
    }

    private TextView getAddressTextView() {
        if (textView == null) {
            textView = activity.findViewById(R.id.txt_address);
        }
        return textView;
    }

    private View getControlPanelWhenEnabled() {
        if (controlPanelWhenEnabled == null) {
            controlPanelWhenEnabled = activity.findViewById(R.id.txt_control_panel_enabled);
        }
        return controlPanelWhenEnabled;
    }

    private View getGetContolPanelWhenDisabled() {
        if (controlPanelWhenDisabled == null) {
            controlPanelWhenDisabled = activity.findViewById(R.id.txt_control_panel_disabled);
        }
        return controlPanelWhenDisabled;
    }

    private MyModalBottomSheetDialog getBottomSheetFragment() {
        return (MyModalBottomSheetDialog) activity.getSupportFragmentManager()
                .findFragmentByTag("some_tag");

    }

}
