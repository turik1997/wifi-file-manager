package com.chiefcomma.app;

import androidx.multidex.MultiDexApplication;

import com.chiefcomma.app.utils.storage_access_framework.StorageHelper;
import java.util.Arrays;

import dagger.hilt.android.HiltAndroidApp;
import hendrawd.storageutil.library.StorageUtil;

@HiltAndroidApp
public class WfmApplication extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        StorageHelper.init(
                Arrays.asList(StorageUtil.getStorageDirectories(getApplicationContext())),
                getApplicationContext()
        );
    }
}
