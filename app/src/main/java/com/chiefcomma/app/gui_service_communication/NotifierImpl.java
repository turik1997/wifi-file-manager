package com.chiefcomma.app.gui_service_communication;

import com.chiefcomma.wfm.interfaces.environment.Notifier;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class NotifierImpl implements Notifier {

    @Inject
    public NotifierImpl() {
    }

    @Override
    public boolean askForConnectionApproval(String clientIp) {
        return false;
    }

}
