package com.chiefcomma.app.network;

import android.annotation.SuppressLint;
import android.net.ConnectivityManager;
import android.net.LinkAddress;
import android.net.LinkProperties;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.chiefcomma.app.utils.ReflectionUtil;
import com.chiefcomma.app.utils.SystemProperties;

import java.math.BigInteger;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class NetworkInterfaceRepository {
    private final WifiManager wifiManager;
    private final ConnectivityManager connectivityManager;

    @Inject
    public NetworkInterfaceRepository(WifiManager wifiManager, ConnectivityManager connectivityManager) {
        this.wifiManager = wifiManager;
        this.connectivityManager = connectivityManager;
    }

    public List<WfmNetworkInterface> getAllInterfaces() {
        List<WfmNetworkInterface> result = new ArrayList<>();
        List<String> mobileDataAddressesAtStart;
        List<String> mobileDataAddressesAtEnd;
        Enumeration<NetworkInterface> networkInterfaces;
        do {
            mobileDataAddressesAtStart = getMobileDataAddresses();
            try {
                networkInterfaces = NetworkInterface.getNetworkInterfaces();
            } catch (SocketException e) {
                return result;
            }
            mobileDataAddressesAtEnd = getMobileDataAddresses();
        } while (!mobileDataAddressesAtStart.containsAll(mobileDataAddressesAtEnd));

        while (networkInterfaces.hasMoreElements()) {
            NetworkInterface networkInterface = networkInterfaces.nextElement();
            try {
                if (!isMobileData(networkInterface, mobileDataAddressesAtEnd) && networkInterface.isUp()) {
                    if (hasIpv4Address(networkInterface)) {
                        result.add(toWfmNetworkInterface(networkInterface));
                    }
                }
            } catch (SocketException e) {
                Log.w("WFM", "Failed to check network interface");
                e.printStackTrace();
            }
        }
        return result;
    }

    private boolean hasIpv4Address(NetworkInterface networkInterface) {
        return getIPv4Address(networkInterface) != null;
    }

    private String getIPv4Address(NetworkInterface networkInterface) {
        for (InterfaceAddress interfaceAddress : networkInterface.getInterfaceAddresses()) {
            if (isIpv4Address(interfaceAddress.getAddress())) {
                return interfaceAddress.getAddress().getHostAddress();
            }
        }
        return null;
    }

    private String getIPv4Address(LinkProperties linkProperties) {
        Collection<InetAddress> inetAddresses = getInetAddresses(linkProperties);
        for (InetAddress inetAddress : inetAddresses) {
            if (isIpv4Address(inetAddress) && !inetAddress.isLoopbackAddress()) {
                return inetAddress.getHostAddress();
            }
        }
        return null;
    }

    @NonNull
    private List<InetAddress> getInetAddresses(LinkProperties linkProperties) {
        List<InetAddress> result = new ArrayList<>();
        Collection<LinkAddress> linkAddresses = getLinkAddresses(linkProperties);
        if (linkAddresses != null) {
            for (LinkAddress linkAddress : linkAddresses) {
                result.add(getInetAddress(linkAddress));
            }
            return result;
        }
        Collection<InetAddress> inetAddresses = (Collection<InetAddress>) ReflectionUtil.withFallbacks(
                () -> ReflectionUtil.callMethod(linkProperties, "getAddresses")
        );
        if (inetAddresses != null) {
            result.addAll(inetAddresses);
        }
        return result;
    }

    private InetAddress getInetAddress(LinkAddress linkAddress) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return linkAddress.getAddress();
        }
        return (InetAddress) ReflectionUtil.withFallbacks(
                () -> ReflectionUtil.callMethod(linkAddress, "getAddress")
        );
    }

    private Collection<LinkAddress> getLinkAddresses(LinkProperties linkProperties) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return linkProperties.getLinkAddresses();
        }
        return (Collection<LinkAddress>) ReflectionUtil.withFallbacks(
                () -> ReflectionUtil.callMethod(linkProperties, "getLinkAddresses")
        );
    }

    private boolean isIpv4Address(InetAddress inetAddress) {
        return inetAddress instanceof Inet4Address;
    }

//    private Network[] getActiveNetworks() {
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
//            return connectivityManager.getAllNetworks();
//        }
//        Network[] allNetworks = (Network[]) ReflectionUtil.withFallbacks(
//                () -> ReflectionUtil.callMethod(connectivityManager, "getAllNetworks")
//        );
//        if (allNetworks == null) {
//            allNetworks = new Network[0];
//        }
//        return allNetworks;
//    }

    private boolean isMobileData(
            NetworkInterface networkInterface,
            List<String> mobileDataIpAddresses
    ) {
        if (mobileDataIpAddresses.isEmpty()) {
            return false;
        }
        Enumeration<InetAddress> inetAddresses = networkInterface.getInetAddresses();
        while (inetAddresses.hasMoreElements()) {
            InetAddress inetAddress = inetAddresses.nextElement();
            for (String mobileDataIpAddress : mobileDataIpAddresses) {
                if (mobileDataIpAddress.equalsIgnoreCase(inetAddress.getHostAddress())) {
                    return true;
                }
            }
        }
        return false;
    }

    @SuppressLint("NewApi")
    private List<String> getMobileDataAddresses() {
        List<String> result = new ArrayList<>();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            for (Network network : connectivityManager.getAllNetworks()) {
                NetworkInfo networkInfo = connectivityManager.getNetworkInfo(network);
                if (networkInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
                    String ipv4 = extractIpv4AddressFromMobileNetwork(network);
                    if (ipv4 != null) {
                        result.add(ipv4);
                    }
                }
            }
            return result;
        }
        try {
            Boolean isMobileDataEnabled = (Boolean) ReflectionUtil.callMethod(connectivityManager, "getMobileDataEnabled");
            if (Boolean.FALSE.equals(isMobileDataEnabled)) {
                return new ArrayList<>();
            }
            LinkProperties linkProperties = (LinkProperties) ReflectionUtil.callMethod(connectivityManager, "getActiveLinkProperties");
            if (linkProperties != null) {
                result.add(getIPv4Address(linkProperties));
                return result;
            }
        } catch (Exception exception) {}
        return result;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private String extractIpv4AddressFromMobileNetwork(Network mobileNetwork) {
        LinkProperties linkProperties = connectivityManager.getLinkProperties(mobileNetwork);
        if (linkProperties != null) {
            return getIPv4Address(linkProperties);
        }
        return null;
    }

    private WfmNetworkInterface toWfmNetworkInterface(NetworkInterface networkInterface) {
        WfmNetworkInterface wfmNetworkInterface = new WfmNetworkInterface();
        wfmNetworkInterface.setName(networkInterface.getName());
        wfmNetworkInterface.setDisplayedName(networkInterface.getDisplayName());
        wfmNetworkInterface.setIpAddress(getIPv4Address(networkInterface));
        wfmNetworkInterface.setNetworkInterfaceType(getNetworkInterfaceType(networkInterface));
        wfmNetworkInterface.setNetworkInterface(networkInterface);
        return wfmNetworkInterface;
    }

    public NetworkInterfaceType getNetworkInterfaceType(NetworkInterface networkInterface) {
        if (isLoopback(networkInterface)) {
            return NetworkInterfaceType.LOOPBACK;
        }
        if (isWifi(networkInterface)) {
            return NetworkInterfaceType.WIFI;
        }
        if (isWifiTethering(networkInterface)) {
            return NetworkInterfaceType.WIFI_HOTSPOT;
        }
        if (isBluetoothTethering(networkInterface)) {
            return NetworkInterfaceType.BLUETOOTH_TETHERING;
        }

        if (isUsbTethering(networkInterface)) {
            return NetworkInterfaceType.USB_TETHERING;
        }

        return NetworkInterfaceType.OTHER;
    }

    private boolean isUsbTethering(NetworkInterface networkInterface) {
        try {
            String[] patterns = (String[]) ReflectionUtil.callMethod(connectivityManager, "getTetherableUsbRegexs");
            if (patterns != null && patterns.length > 0) {
                return networkInterfaceNameMatchesPatterns(networkInterface.getName(), patterns);
            }
        } catch (Exception ex) {}
        String usbStatesValue = SystemProperties.read("sys.usb.state");
        if (usbStatesValue != null) {
            String[] usbStates = usbStatesValue.split(",");
            return networkInterfaceNameMatchesPatterns(networkInterface.getName(), usbStates);
        }

        return false;
    }

    private boolean isBluetoothTethering(NetworkInterface networkInterface) {
        try {
            String[] patterns = (String[]) ReflectionUtil.callMethod(connectivityManager, "getTetherableBluetoothRegexs");
            if (patterns != null && patterns.length > 0) {
                return networkInterfaceNameMatchesPatterns(networkInterface.getName(), patterns);
            }
        } catch (Exception ex) {}
        return false;
    }

    private boolean isWifiTethering(NetworkInterface networkInterface) {
        try {
            String[] patterns = (String[]) ReflectionUtil.callMethod(connectivityManager, "getTetherableWifiRegexs");
            if (patterns != null && patterns.length > 0) {
                return networkInterfaceNameMatchesPatterns(networkInterface.getName(), patterns);
            }
        } catch (Exception ex) {}
        String hotspotNameValue = SystemProperties.read("vendor.wifi.dualconcurrent.interface");
        if (hotspotNameValue != null) {
            String[] hotspotNames = hotspotNameValue.split(",");
            return networkInterfaceNameMatchesPatterns(networkInterface.getName(), hotspotNames);
        }
        return false;
    }

    private boolean isLoopback(NetworkInterface networkInterface) {
        try {
            return networkInterface.isLoopback();
        } catch (SocketException e) {}
        String iPv4Address = getIPv4Address(networkInterface);
        return iPv4Address != null && iPv4Address.equalsIgnoreCase("127.0.0.1");
    }

    public boolean isWifi(NetworkInterface networkInterface) {
        String wifiInterfaceAddress = getWifiInterfaceAddress();
        return wifiInterfaceAddress != null
                && Objects.equals(wifiInterfaceAddress, getIPv4Address(networkInterface));
    }

    private String getWifiInterfaceAddress() {
        int ipAddress = wifiManager.getConnectionInfo().getIpAddress();
        if (ipAddress != 0) {
            try {
                byte[] byteArray = BigInteger.valueOf(ipAddress).toByteArray();
                byte[] bytes = new byte[] { byteArray[3], byteArray[2], byteArray[1], byteArray[0]};
                return InetAddress.getByAddress(bytes).getHostAddress();
            } catch (UnknownHostException e) {
                return null;
            }
        }
        return null;
    }

    private boolean networkInterfaceNameMatchesPatterns(String networkInterfaceName, String... patterns) {
        for (String pattern : patterns) {
            if (networkInterfaceName.matches(pattern) || networkInterfaceName.toLowerCase().contains(pattern.toLowerCase())) {
                return true;
            }
        }
        return false;
    }
}
