package com.chiefcomma.app.network;

import android.os.Build;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class NetworkService {

    private final NetworkInterfaceRepository networkInterfaceRepository;

    @Inject
    public NetworkService(NetworkInterfaceRepository networkInterfaceRepository) {
        this.networkInterfaceRepository = networkInterfaceRepository;
    }

    public WfmNetworkInterface getMostPreferredNetwork() {
        List<WfmNetworkInterface> networkInterfaces = getNetworkInterfaces();
        if (networkInterfaces.isEmpty()) {
            return null;
        }
        return networkInterfaces.get(0);
    }

    public List<WfmNetworkInterface> getNetworkInterfaces() {
        List<WfmNetworkInterface> networkInterfaces = networkInterfaceRepository.getAllInterfaces();
        Collections.sort(networkInterfaces, (ni1, ni2) -> ni1.getNetworkInterfaceType().ordinal() - ni2.getNetworkInterfaceType().ordinal());
        return networkInterfaces;
    }
}
