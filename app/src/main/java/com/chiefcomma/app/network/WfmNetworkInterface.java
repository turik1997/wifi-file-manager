package com.chiefcomma.app.network;

import java.net.NetworkInterface;

public class WfmNetworkInterface {

    private String name;
    private String displayedName;
    private NetworkInterfaceType networkInterfaceType;
    private String ipAddress;
    private NetworkInterface networkInterface;

    void setName(String name) {
        this.name = name;
    }

    void setNetworkInterfaceType(NetworkInterfaceType networkInterfaceType) {
        this.networkInterfaceType = networkInterfaceType;
    }

    void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    void setDisplayedName(String displayedName) {
        this.displayedName = displayedName;
    }

    void setNetworkInterface(NetworkInterface networkInterface) {
        this.networkInterface = networkInterface;
    }

    public String getName() {
        return name;
    }

    public NetworkInterfaceType getNetworkInterfaceType() {
        return networkInterfaceType;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public String getDisplayedName() {
        return displayedName;
    }

    public NetworkInterface getNetworkInterface() {
        return networkInterface;
    }

    @Override
    public String toString() {
        return "WfmNetworkInterface{" +
                "name='" + name + '\'' +
                ", displayedName='" + displayedName + '\'' +
                ", networkInterfaceType=" + networkInterfaceType +
                ", ipAddress='" + ipAddress + '\'' +
                ", networkInterface=" + networkInterface +
                '}';
    }
}
