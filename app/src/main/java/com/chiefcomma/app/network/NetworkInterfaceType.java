package com.chiefcomma.app.network;

public enum NetworkInterfaceType {

    WIFI,
    WIFI_HOTSPOT,
    USB_TETHERING,
    BLUETOOTH_TETHERING,
    OTHER,
    LOOPBACK
}
