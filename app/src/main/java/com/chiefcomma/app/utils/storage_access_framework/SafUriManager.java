package com.chiefcomma.app.utils.storage_access_framework;

import static com.chiefcomma.app.utils.storage_access_framework.StorageHelper.SHARED_PREFS_URIS_KEY;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.provider.DocumentsContract;

import androidx.annotation.Nullable;
import androidx.core.provider.DocumentsContractCompat;
import androidx.documentfile.provider.DocumentFile;

import com.chiefcomma.app.config.qualifier.UriSharedPreferences;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.hilt.android.qualifiers.ApplicationContext;

@Singleton
public class SafUriManager {

    private static final String TEMP_DIRS = "tempDirs";
    private final Context applicationContext;
    private final SharedPreferences uriSharedPreferences;

    @Inject
    public SafUriManager(
            @ApplicationContext Context applicationContext,
            @UriSharedPreferences SharedPreferences uriSharedPreferences
    ) {
        this.applicationContext = applicationContext;
        this.uriSharedPreferences = uriSharedPreferences;
    }

    public void removeTempDirs() {
        Set<String> tempDirs = getTempDirs();
        for (String tempDir : tempDirs) {
            File file = new File(tempDir);
            Uri previouslySavedTreeUri = getPreviouslySavedTreeUri(file.getParentFile());
            if (previouslySavedTreeUri != null) {
                DocumentFile documentFile = DocumentFile.fromTreeUri(applicationContext, previouslySavedTreeUri);
                DocumentFile createdDir = documentFile.findFile(file.getName());
                if (createdDir != null) {
                    cleanUp(previouslySavedTreeUri, file, createdDir.getUri());
                }
            }
        }
        saveTempDirs(new HashSet<>());
    }

    public Uri getPreviouslySavedTreeUri(File storageVolumeFile) {
        SharedPreferences treeUrisSharedPrefs = applicationContext.getSharedPreferences(SHARED_PREFS_URIS_KEY, Context.MODE_PRIVATE);
        String treeUriString = treeUrisSharedPrefs.getString(storageVolumeFile.getAbsolutePath(), null);
        if (treeUriString != null) {
            return Uri.parse(treeUriString);
        }
        return null;
    }

    public void storeUriPermissionForVolume(File storageVolumeFile, Uri treeUri) {
        uriSharedPreferences.edit()
                .putString(storageVolumeFile.getAbsolutePath(), treeUri.toString())
                .apply();
    }

    public boolean treeUriPointsToStorageVolume(File storageVolumeFile, Uri treeUri) {
        // simply checking  DocumentFile.fromFile(storageVolumeFile).equals(treeUri) won't work
        // file:///storage/1EF9-2B09 vs content://com.android.externalstorage.documents/tree/1EF9-2B09%3A
        File tempDirFileToCreate = buildRandomTempDirectoryName(storageVolumeFile);
        Uri createdDirUri = tryCreateTempDirectory(treeUri, tempDirFileToCreate);
        boolean result = tempDirFileToCreate.exists();
        if (createdDirUri != null) {
            cleanUp(treeUri, tempDirFileToCreate, createdDirUri);
        }
        return result;
    }

    @Nullable
    private Uri tryCreateTempDirectory(Uri treeUri, File tempDirFileToCreate) {
        storeTempDir(tempDirFileToCreate.getAbsolutePath());
        Uri parentDocumentUri = DocumentFile.fromTreeUri(applicationContext, treeUri).getUri();
        try {
            return DocumentsContractCompat.createDocument(
                    applicationContext.getContentResolver(),
                    parentDocumentUri,
                    DocumentsContract.Document.MIME_TYPE_DIR,
                    tempDirFileToCreate.getName()
            );
        } catch (FileNotFoundException e) {
            return null;
        }
    }

    private File buildRandomTempDirectoryName(File storageVolumeFile) {
        File tempDirFileToCreate;
        do {
            String uuid = UUID.randomUUID().toString();
            tempDirFileToCreate = new File(storageVolumeFile, uuid);
        } while (tempDirFileToCreate.exists());
        return tempDirFileToCreate;
    }

    private boolean deleteCreatedDir(Uri tempDirUri) throws FileNotFoundException {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            return DocumentsContract.deleteDocument(
                    applicationContext.getContentResolver(),
                    tempDirUri
            );
        }
        return false;
    }

    private void cleanUp(Uri treeUri, File tempDirFileToCreate, Uri createdDirUri) {
        boolean deleteSucceeded;
        try {
            deleteSucceeded = deleteCreatedDir(createdDirUri);
        } catch (FileNotFoundException e) {
            deleteSucceeded = false;
        }
        if (deleteSucceeded) {
            // System.out.println("Deleted temp dir successfully: " + tempDirFileToCreate.getName());
            removeTempDir(tempDirFileToCreate.getAbsolutePath());
        }
    }

    private void storeTempDir(String tempDirAbsolutePath) {
        Set<String> tempDirs = getTempDirs();
        tempDirs.add(tempDirAbsolutePath);
        saveTempDirs(tempDirs);
    }

    private void removeTempDir(String tempDirAbsolutePath) {
        Set<String> tempDirs = getTempDirs();
        tempDirs.remove(tempDirAbsolutePath);
        saveTempDirs(tempDirs);
    }

    private Set<String> getTempDirs() {
        return uriSharedPreferences.getStringSet(TEMP_DIRS, new HashSet<>());
    }

    private void saveTempDirs(Set<String> tempDirs) {
        uriSharedPreferences.edit()
                .putStringSet(TEMP_DIRS, tempDirs)
                .apply();

    }
}
