package com.chiefcomma.app.utils;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class ResourceUtil {

    private final static Map<Integer, String> rawData = new HashMap<>();
    private final static Map<String, String> textsCache = new HashMap<>();


    public final static String readRawData(Context context, int id) {
        String cachedResult = rawData.get(id);
        if (cachedResult != null) {
            return cachedResult;
        }
        InputStream is = context.getResources().openRawResource(id);

        StringBuilder builder = new StringBuilder();

        int read = -1;
        byte[] buf = new byte[1024];
        try {
            while ( (read = is.read(buf, 0, buf.length)) != -1 )
            {
                String string = new String(buf, 0, read);
                builder.append(string);
            }
        } catch (IOException e) {
            return null;
        }

        String result = builder.toString();
        rawData.put(id, result);
        return result;
    }
}
