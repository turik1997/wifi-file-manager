package com.chiefcomma.app.utils;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.List;

public class NetworkHelper {

    public final static String getIp() throws SocketException {
        String tempExternalAddress = null;
        List<NetworkInterface> netInterfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
        for ( NetworkInterface netInterface : netInterfaces )
        {

            List<InetAddress> addresses = Collections.list(netInterface.getInetAddresses());
            for ( InetAddress address : addresses )
            {
                if ( address.isLoopbackAddress() )
                    continue;
                String sAddr = address.getHostAddress();
                System.out.println(sAddr);
                //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                boolean isIPv4 = sAddr.indexOf(':')<0;
                if ( !isIPv4 )
                    continue;

                if ( isIpInPrivateNetwork(sAddr) )
                    return sAddr;
                tempExternalAddress = sAddr;
            }
        }

        if ( tempExternalAddress == null )
            return "127.0.0.1";
        return tempExternalAddress;
    }

    private static boolean isIpInPrivateNetwork(String ip) {
        return ip.startsWith("192.168.");
    }

}
