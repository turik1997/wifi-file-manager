package com.chiefcomma.app.utils;

import android.app.Activity;
import android.text.Html;
import android.text.Spanned;

import androidx.appcompat.app.AppCompatActivity;

import com.chiefcomma.app.R;
import com.chiefcomma.app.gui.main_activity.StorageListAdapter;
import com.chiefcomma.app.gui.dialogs.AboutDialog;
import com.chiefcomma.app.gui.dialogs.ChooseStorageDialog;
import com.chiefcomma.app.gui.dialogs.StorageAccessFrameworkRationaleDialog;
import com.chiefcomma.app.storage.repository.WfmStorageRepository;
import com.chiefcomma.app.storage.repository.WfmStorageVolume;

public class DialogHelper {

    private static volatile Boolean canShowAboutDialog = true;

    public final static void showAboutDialog(Activity activity) {
        // whole hassle is needed to speed up showing dialog. Otherwise it is too slow
        new Thread(() -> {
            synchronized (DialogHelper.class) {
                if (canShowAboutDialog) {
                    canShowAboutDialog = false;
                } else {
                    return;
                }
            }
            Spanned contactContent = Html.fromHtml(ResourceUtil.readRawData(activity, R.raw.contact));
            activity.runOnUiThread(() -> {
                AboutDialog dialog = new AboutDialog(activity);
                dialog.setContactContent(contactContent);
                dialog.setContentView(R.layout.about_dialog_layout);
                dialog.setOnDismissListener(dlg -> canShowAboutDialog = true);
                if (!activity.isFinishing()) {
                    dialog.show();
                } else {
                    canShowAboutDialog = true;
                }
            });
        }).start();
    }

    public final static void showChooseStorageDialog(
            Activity activity,
            WfmStorageRepository wfmStorageRepository,
            StorageListAdapter storageListAdapter
    ) {
        ChooseStorageDialog dialog = new ChooseStorageDialog(activity, wfmStorageRepository, storageListAdapter);
        dialog.show();
    }

    public final static void showStorageAccessFrameworkRationaleDialog(
            AppCompatActivity activity,
            WfmStorageVolume wfmStorageVolume
    ) {
        StorageAccessFrameworkRationaleDialog dialog = new StorageAccessFrameworkRationaleDialog(
                activity,
                wfmStorageVolume
        );
        dialog.show(activity.getSupportFragmentManager(), "SAF_DIALOG");
    }

}
