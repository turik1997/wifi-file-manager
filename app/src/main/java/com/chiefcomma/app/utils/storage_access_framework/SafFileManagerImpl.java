package com.chiefcomma.app.utils.storage_access_framework;

import android.content.Context;
import android.net.Uri;
import android.provider.DocumentsContract;

import androidx.core.provider.DocumentsContractCompat;
import androidx.documentfile.provider.DocumentFile;

import com.chiefcomma.wfm.SafFileManager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

public class SafFileManagerImpl implements SafFileManager {
    private final StorageHelper storageHelper;
    private final Context applicationContext;

    public SafFileManagerImpl(Context applicationContext, StorageHelper storageHelper) {
        this.storageHelper = storageHelper;
        this.applicationContext = applicationContext;
    }

    @Override
    public boolean mkdir(File file) {
        try {
            Uri parentSafUri = storageHelper.getUriForFile(file.getParentFile());
            Uri newDocument = DocumentsContractCompat.createDocument(
                    applicationContext.getContentResolver(),
                    parentSafUri,
                    DocumentsContract.Document.MIME_TYPE_DIR,
                    file.getName()
            );
            return newDocument != null;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean renameTo(File originalFile, String newName) {
        try {
            Uri safUri = storageHelper.getUriForFile(originalFile);
            Uri renamedDocumentUri = DocumentsContractCompat.renameDocument(
                    applicationContext.getContentResolver(),
                    safUri,
                    newName
            );
            return renamedDocumentUri != null;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean canWrite(File file) {
        try {
            Uri safUri = storageHelper.getUriForFile(file);
            if (safUri != null) {
                DocumentFile documentFile = DocumentFile.fromSingleUri(applicationContext, safUri);
                return documentFile != null && documentFile.canWrite();
            }
            return false;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean delete(File file) {
        try {
            Uri safUri = storageHelper.getUriForFile(file);
            DocumentFile documentFile = DocumentFile.fromSingleUri(applicationContext, safUri);
            return documentFile == null || documentFile.delete();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public InputStream getInputStream(File file) throws FileNotFoundException {
        Uri safUri = storageHelper.getUriForFile(file);
        if (safUri == null) {
            throw new FileNotFoundException("Invalid file path: " + safUri);
        }
        return applicationContext.getContentResolver().openInputStream(safUri);
    }

    @Override
    public OutputStream getOutputStream(File file) throws FileNotFoundException {
        Uri safUri = storageHelper.getUriForFile(file);
        if (!DocumentFile.fromSingleUri(applicationContext, safUri).exists()) {
            safUri = DocumentsContractCompat.createDocument(
                    applicationContext.getContentResolver(),
                    storageHelper.getUriForFile(file.getParentFile()),
                    null,
                    file.getName()
            );
        }
        return applicationContext.getContentResolver().openOutputStream(safUri);
    }
}
