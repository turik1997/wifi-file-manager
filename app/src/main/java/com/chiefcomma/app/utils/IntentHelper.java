package com.chiefcomma.app.utils;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.documentfile.provider.DocumentFile;

import com.chiefcomma.app.gui.settings_activity.SettingsActivity;

import java.io.File;

public class IntentHelper {

    private static final String EXTRA_SHOW_ADVANCED = "android.content.extra.SHOW_ADVANCED";
    private static final String EXTRA_INITIAL_URI = "android.provider.extra.INITIAL_URI";


    public final static Intent openUiPickerToSelectRightStorage(Intent intent) {
        return intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION)
                .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                .addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
    }

    public final static Intent openUiPickerToSelectRightStorage(File file) {
        DocumentFile documentFile = DocumentFile.fromFile(file);
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE)
                .putExtra(EXTRA_INITIAL_URI, documentFile.getUri())
                .putExtra(EXTRA_SHOW_ADVANCED, true);
        return openUiPickerToSelectRightStorage(intent);
    }

    public final static boolean tryFullStorageAccessSettingsActivity(Activity activity, String permission) {
        // https://stackoverflow.com/questions/66067616/how-to-access-permission-allow-management-of-all-file-in-android-studio
        Intent settingsIntent = buildActionIntent(permission, activity.getPackageName());
        ComponentName settingsActivity = settingsIntent.resolveActivity(activity.getPackageManager());
        if (settingsActivity != null) {
            activity.startActivity(settingsIntent);
        }
        return settingsActivity != null;
    }

    public final static void startSettingsActivity(Context context) {
        context.startActivity(new Intent(context, SettingsActivity.class));
    }

    private static Intent buildActionIntent(String action, String packageName) {
        return new Intent(action, Uri.fromParts("package", packageName, null));
    }
}
