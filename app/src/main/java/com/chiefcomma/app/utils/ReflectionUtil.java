package com.chiefcomma.app.utils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.Callable;

public final class ReflectionUtil {

    public static Object withFallbacks(Callable<Object>... callables) {
        for (Callable<Object> callable : callables) {
            try {
                return callable.call();
            } catch (Exception e) {}
        }
        return null;
    }

    public static Object callMethod(
            Object subject,
            String methodName,
            Class<?>[] classes,
            Object[] params
    ) throws NoSuchMethodException,
            RuntimeException,
            InvocationTargetException,
            IllegalAccessException {
        Method declaredMethod = subject.getClass().getDeclaredMethod(methodName, classes);
        return invokeMethod(declaredMethod, subject, params);
    }

    public static Object callMethod(
            Object subject,
            String methodName
    ) throws NoSuchMethodException,
            RuntimeException,
            InvocationTargetException,
            IllegalAccessException {
        Method declaredMethod = subject.getClass().getDeclaredMethod(methodName);
        return invokeMethod(declaredMethod, subject);
    }

    public static Object callStaticMethod(
            Class<?> clazz,
            String methodName,
            Object... params
    ) throws NoSuchMethodException,
            RuntimeException,
            InvocationTargetException,
            IllegalAccessException {
        Class<?>[] classes = new Class[params.length];
        for (int i = 0; i < params.length; i++) {
            classes[i] = params[i].getClass();
        }
        Method declaredMethod = clazz.getDeclaredMethod(methodName, classes);
        return invokeMethod(declaredMethod, null, params);
    }

    private static Object invokeMethod(Method method, Object subject, Object... params) throws InvocationTargetException, IllegalAccessException {
        method.setAccessible(true);
        return method.invoke(subject, params);
    }

    public static Object readField(
            Object subject,
            String fieldName
    ) throws NoSuchFieldException,
            IllegalAccessException {
        Field mRemovable = subject.getClass().getDeclaredField(fieldName);
        mRemovable.setAccessible(true);
        return mRemovable.get(subject);
    }
}
