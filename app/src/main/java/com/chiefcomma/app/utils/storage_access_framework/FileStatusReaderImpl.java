package com.chiefcomma.app.utils.storage_access_framework;

import com.chiefcomma.app.utils.storage_access_framework.StorageHelper;
import com.chiefcomma.wfm.FileStatusReader;

import java.io.File;

public class FileStatusReaderImpl implements FileStatusReader {

    private final StorageHelper storageHelper;

    public FileStatusReaderImpl(StorageHelper storageHelper) {
        this.storageHelper = storageHelper;
    }

    @Override
    public FileStatus read(File file) {
        if (storageHelper.getPathMarker(file) == StorageHelper.PathMarker.NEEDS_STORAGE_ACCESS_FRAMEWORK) {
            return FileStatus.SAF;
        }
        return FileStatus.NON_SAF;
    }
}
