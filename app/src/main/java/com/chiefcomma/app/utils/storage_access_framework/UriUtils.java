package com.chiefcomma.app.utils.storage_access_framework;

import static com.chiefcomma.app.utils.storage_access_framework.StorageHelper.SHARED_PREFS_URIS_KEY;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import java.io.File;

public class UriUtils {

    public final static Uri getPreviouslySavedTreeUri(Context applicationContext, File rootStorageFile) {
        SharedPreferences treeUrisSharedPrefs = applicationContext.getSharedPreferences(SHARED_PREFS_URIS_KEY, Context.MODE_PRIVATE);
        String treeUriString = treeUrisSharedPrefs.getString(rootStorageFile.getAbsolutePath(), null);
        if (treeUriString != null) {
            return Uri.parse(treeUriString);
        }
        return null;
    }
}
