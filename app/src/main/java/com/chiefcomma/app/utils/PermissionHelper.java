package com.chiefcomma.app.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;

import androidx.activity.result.ActivityResultLauncher;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.PermissionChecker;

import com.chiefcomma.app.gui.dialogs.PermissionRationaleDialog;

import java.util.Arrays;
import java.util.List;

public class PermissionHelper {

    private final Runnable onPermissionGranted;
    private final Runnable onPermissionDenied;
    private final ActivityResultLauncher<String> requestPermissionLauncher;
    private final Activity activity;
    private boolean displayedRationaleOnce;
    private boolean displayedPermissionDialog;

    public PermissionHelper(
            Runnable onPermissionGranted,
            Runnable onPermissionDenied,
            ActivityResultLauncher<String> requestPermissionLauncher,
            Activity activity
    ) {
        this.onPermissionGranted = onPermissionGranted;
        this.onPermissionDenied = onPermissionDenied;
        this.requestPermissionLauncher = requestPermissionLauncher;
        this.activity = activity;
    }

    public final static boolean isStoragePermissionGranted(Context applicationContext) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            return Environment.isExternalStorageManager();
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return applicationContext.checkSelfPermission(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED;
        } else {
            return PermissionChecker.checkSelfPermission(
                    applicationContext,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) == PermissionChecker.PERMISSION_GRANTED;
        }
    }

    public void checkNecessaryPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            // Starting from API 30, we ask differently for full storage access
            // https://developer.android.com/training/data-storage
            // https://developer.android.com/training/data-storage/manage-all-files
            checkFullStorageAccessForApi30();
        } else {
            checkClassicWriteExternalStorageAccess();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.R)
    private void checkFullStorageAccessForApi30() {
        if (Environment.isExternalStorageManager()) {
            onPermissionGranted.run();
        } else if (!displayedRationaleOnce) {
            displayedRationaleOnce = true;
            PermissionRationaleDialog.showRationaleDialog(
                    this.activity,
                    (dlg, btnNeg) -> onPermissionDenied.run(),
                    (dlg, btnPos) -> {
                        if (!openSettingsToEnableFullStorageAccess()) {
                            onPermissionDenied.run();
                        }
                    }
            );
        } else {
            onPermissionDenied.run();
        }
    }

    private boolean openSettingsToEnableFullStorageAccess() {
        List<String> permissionList = Arrays.asList(
                Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION,
                Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION
        );
        for (String permission : permissionList) {
            if (IntentHelper.tryFullStorageAccessSettingsActivity(this.activity, permission)) {
                return true;
            }
        }
        return false;
    }

    private void checkClassicWriteExternalStorageAccess() {
        String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        if (isPermissionGranted(writeExternalStorage)) {
            this.onPermissionGranted.run();
        } else if (!displayedRationaleOnce && ActivityCompat.shouldShowRequestPermissionRationale(this.activity, writeExternalStorage)) {
            displayedRationaleOnce = true;
            PermissionRationaleDialog.showRationaleDialog(
                    this.activity,
                    (dlg, btnNeg) -> onPermissionDenied.run(),
                    (dlg, btnPos) -> launchPermissionDialog()
            );
        } else if (!displayedPermissionDialog) {
            displayedPermissionDialog = true;
            launchPermissionDialog();
        }
    }

    public boolean isPermissionGranted(String permission) {
        return ContextCompat.checkSelfPermission(
                this.activity,
                permission
        ) == PackageManager.PERMISSION_GRANTED;
    }

    private void launchPermissionDialog() {
        requestPermissionLauncher.launch(Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    public void reset() {
        displayedPermissionDialog = false;
        displayedRationaleOnce = false;
    }
}
