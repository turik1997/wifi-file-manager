package com.chiefcomma.app.utils.storage_access_framework;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import androidx.documentfile.provider.DocumentFile;

import com.chiefcomma.app.utils.PermissionHelper;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.UUID;

public class StorageHelper {

    public enum PathMarker {
        NO_PERMISSION_GRANTED,
        ACCESS_DENIED,
        SYSTEM_FILE,
        NEEDS_STORAGE_ACCESS_FRAMEWORK,
        PHONE_STORAGE,
        UNKNOWN_STATE,
        ERROR,
    }

    public final static String SHARED_PREFS_URIS_KEY = "storage_uris";

    private List<String> storagePaths;
    private Context applicationContext;
    private static StorageHelper storageHelperInstance;

    private StorageHelper(List<String> storagePaths, Context applicationContext) {
        this.storagePaths = storagePaths;
        this.applicationContext = applicationContext;
    }

    public static void init(List<String> storagePaths, Context applicationContext) {
        storageHelperInstance = new StorageHelper(storagePaths, applicationContext);
    }

    public static StorageHelper getInstance() {
        return storageHelperInstance;
    }

    public Uri getUriForFile(File file) throws FileNotFoundException {
        String rootStoragePath = getRootStoragePathOrFail(file);
        Uri rootTreeUri = UriUtils.getPreviouslySavedTreeUri(applicationContext, new File(rootStoragePath));
        if (rootTreeUri != null) {
            File relativeFile = new File(file.getAbsolutePath().substring((rootStoragePath).length()));
            Uri fileUri = copyPathToUri(rootTreeUri, relativeFile);
            return fileUri;
        }
        return null;
    }

    private Uri copyPathToUri(Uri treeUri, File relativeToRootFile) {
        DocumentFile rootDocumentFile = DocumentFile.fromTreeUri(applicationContext, treeUri);
        if (rootDocumentFile != null) {
            String pathWithoutLeadingSlash = relativeToRootFile.getAbsolutePath().substring(1);
            Uri.Builder uriBuilder = rootDocumentFile.getUri().buildUpon();
            uriBuilder.encodedPath(rootDocumentFile.getUri().getEncodedPath() + Uri.encode(pathWithoutLeadingSlash));
            return uriBuilder.build();
        }

        return null;
    }

    public PathMarker getPathMarker(File file) {
        if (file == null) {
            return PathMarker.ERROR;
        }
        try {
            if (!PermissionHelper.isStoragePermissionGranted(applicationContext)) {
                return PathMarker.NO_PERMISSION_GRANTED;
            }

            if (file.exists()) {
                if (!file.canRead()) {
                    return PathMarker.ACCESS_DENIED;
                }
                if (!file.canWrite()) {
                    return PathMarker.SYSTEM_FILE;
                }
            } else {
                // takes into account the case when dest file does not exist,
                // e.g. opening output stream for dest file
                do {
                    file = file.getParentFile();
                } while (file != null && !file.exists());
                return getPathMarker(file);
            }
            if (!tryToWrite(file)) {
                return PathMarker.NEEDS_STORAGE_ACCESS_FRAMEWORK;
            }
        } catch (UnknownError unknownError) {
            unknownError.printStackTrace();
            return PathMarker.UNKNOWN_STATE;
        } catch (Exception exception) {
            exception.printStackTrace();
            return PathMarker.ERROR;
        }

        return PathMarker.PHONE_STORAGE;
    }

    private boolean tryToWrite(File file) {
        String uuid = UUID.randomUUID().toString();
        SharedPreferences tempSharedPrefs = applicationContext.getSharedPreferences("temp_test_dirs", Context.MODE_PRIVATE);
        tempSharedPrefs.edit().putString("file-" + uuid, "1").apply();
        // file.isDirectory() ? file : file.getParentFile() to avoid accidental writing to file
        // also, we can't create a child of a file, hence we use parentfile
        File tmpDir = new File(file.isDirectory() ? file : file.getParentFile(), uuid);
        boolean mkdirSucceeded = tmpDir.mkdir();
        boolean deleteSucceeded = false;
        if (mkdirSucceeded) {
            deleteSucceeded = tmpDir.delete();
            if (deleteSucceeded) {
                tempSharedPrefs.edit().remove("tmp-" + uuid).apply();
            }
        }
        return mkdirSucceeded && deleteSucceeded;
    }

    public String getRootStoragePathOrFail(File file) {
        for (String rootStoragePath : storagePaths) {
            if (file.getAbsolutePath().startsWith(rootStoragePath)) {
                return rootStoragePath;
            }
        }
        if (file.exists()) {
            throw new UnknownError("Can't get root storage path for the file " + file.getAbsolutePath());
        } else {
            throw new IllegalArgumentException("Can't get root storage path for non-existing file " + file.getAbsolutePath());
        }
    }
}
