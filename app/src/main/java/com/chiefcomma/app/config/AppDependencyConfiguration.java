package com.chiefcomma.app.config;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;

import androidx.preference.PreferenceManager;

import com.chiefcomma.app.config.qualifier.StorageVolumeSharedPreferences;
import com.chiefcomma.app.config.qualifier.UriSharedPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.qualifiers.ApplicationContext;
import dagger.hilt.components.SingletonComponent;

@Module
@InstallIn(SingletonComponent.class)
public class AppDependencyConfiguration {

    private static final String URI_SHARED_PREFERENCES = "storage_uris";
    private static final String STORAGE_VOLUMES_SHARED_PREFERENCES = "storage_volumes";

    @Provides
    @Singleton
    public LayoutInflater layoutInflater(@ApplicationContext Context context) {
        return LayoutInflater.from(context);
    }

    @Provides
    @Singleton
    public SharedPreferences defaultSharedPreferences(@ApplicationContext Context applicationContext) {
        return PreferenceManager.getDefaultSharedPreferences(applicationContext);
    }

    @Provides
    @Singleton
    @UriSharedPreferences
    public SharedPreferences uriSharedPreferences(@ApplicationContext Context applicationContext) {
        return applicationContext.getSharedPreferences(URI_SHARED_PREFERENCES, Context.MODE_PRIVATE);
    }

    @Provides
    @Singleton
    @StorageVolumeSharedPreferences
    public SharedPreferences storageVolumesSharedPreferences(@ApplicationContext Context applicationContext) {
        return applicationContext.getSharedPreferences(STORAGE_VOLUMES_SHARED_PREFERENCES, Context.MODE_PRIVATE);
    }
}
