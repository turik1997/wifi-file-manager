package com.chiefcomma.app.config;

import com.chiefcomma.app.androidservice.interfaces.AuthEncoder;
import com.chiefcomma.app.androidservice.settings.Informer;
import com.chiefcomma.app.androidservice.settings.UserAccessManager;
import com.chiefcomma.app.androidservice.settings.UserAuthManager;
import com.chiefcomma.app.androidservice.settings.ServerConfigs;
import com.chiefcomma.app.gui_service_communication.NotifierImpl;
import com.chiefcomma.wfm.WFMWebService;
import com.chiefcomma.wfm.interfaces.users.validators.UserAccessValidator;
import com.chiefcomma.wfm.request.RequestValidator;
import com.chiefcomma.wfm.server.servlet.security_layer.SecureServer;
import com.chiefcomma.wfm.utils.AssetManager;

import java.util.Locale;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.components.SingletonComponent;

@Module
@InstallIn(SingletonComponent.class)
public abstract class WebDependencyConfiguration {


    @Binds
    @Singleton
    public abstract AuthEncoder authEncoder(ServerConfigs serverConfigs);


    @Provides
    public static AssetManager assetManager(Locale locale) {
        return new AssetManager(locale);
    }

    @Provides
    public static RequestValidator requestValidator(Informer environmentInformer) {
        return new RequestValidator(
                assetManager(environmentInformer.getLocale()),
                environmentInformer
        );
    }

    @Provides
    @Singleton
    public static SecureServer secureServer(
            ServerConfigs serverConfigs,
            UserAccessManager userAccessManager,
            UserAuthManager userAuthManager,
            NotifierImpl notifier,
            Informer informer
    ) {
        return new SecureServer(
                serverConfigs,
                userAccessManager,
                userAuthManager,
                assetManager(informer.getLocale()),
                notifier,
                informer
        );
    }

    @Provides
    @Singleton
    public static WFMWebService wfmWebService(
            ServerConfigs serverConfigs,
            UserAccessManager userAccessManager,
            UserAuthManager userAuthManager,
            Informer informer,
            RequestValidator requestValidator,
            NotifierImpl notifier,
            SecureServer secureServer
    ) {
        return new WFMWebService(
                serverConfigs,
                userAccessManager,
                userAuthManager,
                informer,
                requestValidator,
                notifier,
                secureServer
        );
    }
}
