package com.chiefcomma.app.config;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Build;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.qualifiers.ApplicationContext;
import dagger.hilt.components.SingletonComponent;

@Module
@InstallIn(SingletonComponent.class)
public class NetworkDependencyConfiguration {

    @Provides
    public static WifiManager wifiManager(@ApplicationContext Context applicationContext) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return applicationContext.getSystemService(WifiManager.class);
        }
        return (WifiManager) applicationContext.getSystemService(Context.WIFI_SERVICE);
    }

    @Provides
    public static ConnectivityManager connectivityManager(@ApplicationContext Context applicationContext) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return applicationContext.getSystemService(ConnectivityManager.class);
        }
        return (ConnectivityManager) applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE);
    }
}
