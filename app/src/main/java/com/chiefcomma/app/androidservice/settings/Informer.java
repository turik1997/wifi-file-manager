package com.chiefcomma.app.androidservice.settings;


import android.content.Context;
import android.os.Environment;

import com.chiefcomma.app.R;
import com.chiefcomma.app.storage.StorageType;
import com.chiefcomma.app.storage.WfmStorageService;
import com.chiefcomma.app.storage.repository.WfmStorageVolume;
import com.chiefcomma.wfm.interfaces.environment.EnvironmentInformer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.hilt.android.qualifiers.ApplicationContext;

@Singleton
public class Informer implements EnvironmentInformer {

    private final Context applicationContext;
    private final WfmStorageService wfmStorageService;

    @Inject
    public Informer(
            @ApplicationContext Context applicationContext,
            WfmStorageService wfmStorageService
    ) {
        this.applicationContext = applicationContext;
        this.wfmStorageService = wfmStorageService;
    }

    @Override
    public File getDefaultStorage() {
        List<WfmStorageVolume> enabledStorages = wfmStorageService.getEnabledStorageVolumes();
        if (enabledStorages.isEmpty()) {
            return null;
        }
        File internalStorage = findInternalStorage(enabledStorages);
        if (internalStorage != null) {
            return internalStorage;
        }
        File primaryExternalStorage = findPrimaryExternalStorage(enabledStorages);
        if (primaryExternalStorage != null) {
            return primaryExternalStorage;
        }
        return new File(enabledStorages.get(0).getAbsolutePath());
    }

    @Override
    public List<String> getListOfStorages() {
        List<String> result = new ArrayList<>();
        for (WfmStorageVolume wfmStorageVolume : wfmStorageService.getEnabledStorageVolumes()) {
            result.add(wfmStorageVolume.getAbsolutePath());
        }
        return result;
    }

    @Override
    public Locale getLocale() {
        return new Locale(applicationContext.getString(R.string.locale));
    }

    private File findInternalStorage(List<WfmStorageVolume> enabledStorages) {
        for (WfmStorageVolume wfmStorageVolume : enabledStorages) {
            if (wfmStorageVolume.getStorageType() == StorageType.INTERNAL) {
                return new File(wfmStorageVolume.getAbsolutePath());
            }
        }
        return null;
    }

    private File findPrimaryExternalStorage(List<WfmStorageVolume> enabledStorages) {
        File externalStorageDirectory = Environment.getExternalStorageDirectory();
        if (externalStorageDirectory != null) {
            for (WfmStorageVolume wfmStorageVolume : enabledStorages) {
                if (externalStorageDirectory.getAbsolutePath().startsWith(wfmStorageVolume.getAbsolutePath())) {
                    return new File(wfmStorageVolume.getAbsolutePath());
                }
            }
        }
        return null;
    }
}