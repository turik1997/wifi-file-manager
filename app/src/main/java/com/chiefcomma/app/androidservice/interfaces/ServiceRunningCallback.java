package com.chiefcomma.app.androidservice.interfaces;

public interface ServiceRunningCallback {
    default void onStart() {}
    default void onStop() {}
}
