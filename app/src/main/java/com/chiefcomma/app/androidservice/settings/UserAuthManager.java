package com.chiefcomma.app.androidservice.settings;

import android.util.Log;

import com.chiefcomma.app.androidservice.interfaces.AuthEncoder;
import com.chiefcomma.wfm.interfaces.users.validators.UserAuthValidator;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Hp on 9/20/2017.
 */
@Singleton
public class UserAuthManager implements UserAuthValidator {

    private final AuthEncoder encoder;

    @Inject
    public UserAuthManager(AuthEncoder encoder) {
        this.encoder = encoder;
    }

    @Override
    public boolean isAuthTokenValid(String clientIp, String authToken) {
        Log.d("AuthValidation", "Checking " + authToken);
        if ( authToken == null || authToken.length() <= "Basic ".length() )
            return false;

        authToken = authToken.startsWith("Basic ") ? authToken.substring("Basic ".length()) : authToken;
        Log.d("AuthValidation", "IP: " + clientIp + " AuthToken: " + authToken);
        String encoded = encoder.encode(clientIp);
        Log.d("Encoded", encoded + " are equal: " + encoded.equals(authToken));

        return encoded.equals(authToken);
    }
}
