package com.chiefcomma.app.androidservice.interfaces;

import androidx.preference.Preference;

/**
 * Created by Hp on 9/16/2017.
 */

public interface PreferenceFinder {

    Preference findPreference(CharSequence key);
}
