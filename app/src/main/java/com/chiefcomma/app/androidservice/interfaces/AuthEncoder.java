package com.chiefcomma.app.androidservice.interfaces;

/**
 * Created by Hp on 9/21/2017.
 */

public interface AuthEncoder {

    String encode(String username);
}
