package com.chiefcomma.app.androidservice.interfaces;

/**
 * Created by Hp on 9/19/2017.
 */

public interface UserRightsGranter {

    void grantServerAccess(String ip);
    void denyServerAccess(String ip);
}
