package com.chiefcomma.app.androidservice.interfaces.gui.configurators;

/**
 * Created by Hp on 9/19/2017.
 */

public interface AppConfigurator extends ServerConfigurator, ManualAccessConfigurator {
}
