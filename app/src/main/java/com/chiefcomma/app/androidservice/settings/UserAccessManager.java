package com.chiefcomma.app.androidservice.settings;

import com.chiefcomma.wfm.interfaces.users.validators.UserAccessValidator;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Hp on 9/19/2017.
 */
@Singleton
public class UserAccessManager implements UserAccessValidator {
    @Inject
    public UserAccessManager() {
    }

    @Override
    public boolean isAccessAllowed(String clientIp) {
        return true;
    }

    @Override
    public boolean isAccessProhibited(String clientIp) {
        return false;
    }
}
