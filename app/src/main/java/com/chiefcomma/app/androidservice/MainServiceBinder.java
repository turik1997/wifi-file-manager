package com.chiefcomma.app.androidservice;

import android.os.Binder;

import com.chiefcomma.app.androidservice.interfaces.ServiceRunningCallback;

import java.lang.ref.WeakReference;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class MainServiceBinder extends Binder {

    private volatile WeakReference<ServiceRunningCallback> serviceRunningCallback;

    @Inject
    public MainServiceBinder() {
    }

    public synchronized void setServiceRunningCallback(ServiceRunningCallback serviceRunningCallback) {
        this.serviceRunningCallback = new WeakReference<>(serviceRunningCallback);
    }

    public synchronized ServiceRunningCallback getServiceRunningCallback() {
        if (serviceRunningCallback != null) {
            return serviceRunningCallback.get();
        }
        return null;
    }
}
