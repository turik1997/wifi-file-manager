package com.chiefcomma.app.androidservice.interfaces.gui.configurators;

/**
 * Created by Hp on 9/16/2017.
 */

public interface ServerConfigurator extends Configurator {

    void changePort(int port);
    void changePassword(String password);
}
