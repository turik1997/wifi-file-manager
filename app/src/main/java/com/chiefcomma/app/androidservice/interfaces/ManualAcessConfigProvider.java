package com.chiefcomma.app.androidservice.interfaces;

/**
 * Created by Hp on 9/19/2017.
 */

public interface ManualAcessConfigProvider {

    boolean isManualAccessEnabled();
}
