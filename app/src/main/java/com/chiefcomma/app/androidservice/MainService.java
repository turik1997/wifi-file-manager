package com.chiefcomma.app.androidservice;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import android.util.Log;

import com.chiefcomma.app.R;
import com.chiefcomma.app.androidservice.interfaces.ServiceRunningCallback;
import com.chiefcomma.app.gui.splash_activity.SplashActivity;
import com.chiefcomma.app.storage.repository.WfmStorageRepository;
import com.chiefcomma.app.utils.storage_access_framework.FileStatusReaderImpl;
import com.chiefcomma.app.utils.storage_access_framework.SafFileManagerImpl;
import com.chiefcomma.app.utils.storage_access_framework.StorageHelper;
import com.chiefcomma.wfm.SafFileManager;
import com.chiefcomma.wfm.WFMWebService;

import java.io.IOException;

import static com.chiefcomma.app.gui.main_activity.MainActivity.IS_FROM_NOTIFICATION;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;

/**
 * Created by Turkhan Badalov on 9/12/17.
 */

@AndroidEntryPoint
public class MainService extends Service {

    public static volatile boolean IS_RUNNING = false;
    private SafFileManager safFileManager;
    @Inject
    WfmStorageRepository wfmStorageRepository;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    MainServiceBinder mainServiceBinder;
    @Inject
    WFMWebService service;



    @Override
    public void onCreate() {
        super.onCreate();
        safFileManager = new SafFileManagerImpl(
                getApplicationContext(),
                StorageHelper.getInstance()
        );
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (isServerRunning()) {
            if ("STOP_SERVICE".equals(intent.getAction())) {
                stopSelf();
            }
            return START_NOT_STICKY;
        }
        service.enableStorageAccessFrameworkSupport(
                new FileStatusReaderImpl(StorageHelper.getInstance()),
                safFileManager
        );
        try {
            service.start();
        } catch (IOException e) {
            e.printStackTrace();
        }

        IS_RUNNING = true;
        ServiceRunningCallback serviceRunningCallback = mainServiceBinder.getServiceRunningCallback();
        if (serviceRunningCallback != null) {
            serviceRunningCallback.onStart();
        }
        Intent activityIntent = new Intent(this, SplashActivity.class);
        activityIntent.putExtra(IS_FROM_NOTIFICATION, true);
        activityIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        activityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingAcIntent = PendingIntent.getActivity(getApplicationContext(), 1, activityIntent, PendingIntent.FLAG_IMMUTABLE);
        String channelId = "wfm_channel";
        Intent stopSelf = new Intent(getApplicationContext(), MainService.class);
        stopSelf.setAction("STOP_SERVICE");
        Notification notification = new NotificationCompat.Builder(getApplicationContext(), channelId)
                .setContentTitle("Wifi File Manager")
                .setContentText("Running....")
                .setSmallIcon(R.drawable.wfm_logo_splash)
                .setContentIntent(pendingAcIntent)
                // https://developer.android.com/develop/background-work/services/foreground-services#notification-immediate
                .setForegroundServiceBehavior(NotificationCompat.FOREGROUND_SERVICE_IMMEDIATE)
                .addAction(
                        android.R.drawable.ic_menu_close_clear_cancel,
                        "Stop",
                        PendingIntent.getService(this, 0, stopSelf, PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_IMMUTABLE)
                )
                .setSilent(true)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setPriority(Notification.PRIORITY_MAX)
                .build();
        startForeground(1, notification);

        // https://stackoverflow.com/a/9441795/6657837
        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mainServiceBinder;
    }

    @Override
    public void onDestroy() {
        Log.d("MainService", "OnStop");
        service.stop();
        service = null;
        IS_RUNNING = false;
        ServiceRunningCallback serviceRunningCallback = mainServiceBinder.getServiceRunningCallback();
        if (serviceRunningCallback != null) {
            serviceRunningCallback.onStop();
        }
        super.onDestroy();
    }

    private boolean isServerRunning() {
        return service != null && service.isAlive();
    }

}
