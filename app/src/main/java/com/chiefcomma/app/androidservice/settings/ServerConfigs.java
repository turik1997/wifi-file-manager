package com.chiefcomma.app.androidservice.settings;

import android.content.SharedPreferences;
import android.util.Base64;

import com.chiefcomma.app.androidservice.interfaces.AuthEncoder;
import com.chiefcomma.wfm.interfaces.environment.ConfigProvider;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Hp on 9/16/2017.
 */

@Singleton
public class ServerConfigs implements ConfigProvider, AuthEncoder {

    public static String PORT_KEY = "pref_key_port";
    public static String PASSWORD_KEY = "pref_key_password";

    public final static int DEFAULT_PORT = 1234;
    public final static String DEFAULT_PASSWORD = "";

    private final SharedPreferences sharedPreferences;

    @Inject
    public ServerConfigs(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    @Override
    public long getUploadLimit() {
        return 0;
    }
    @Override
    public int getPort() {
        return Integer.parseInt(
                sharedPreferences.getString(PORT_KEY, Integer.toString(DEFAULT_PORT))
        );
    }

    @Override
    public String encode(String username) {
        String builder = username + ':' + getPassword();
        return Base64.encodeToString(builder.getBytes(), Base64.NO_WRAP);
    }

    @Override
    public boolean isSecuredByPass() {
        String password = getPassword();
        return password != null && password.length() > 0;
    }

    @Override
    public boolean isManualAccessEnabled() {
        return false;
    }

    private String getPassword() {
        return sharedPreferences.getString(PASSWORD_KEY, DEFAULT_PASSWORD);
    }
}
